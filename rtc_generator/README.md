### rtc_generator ###
Use this program to create a \*.rtc file to be placed on the HPR device to update the device's system time.

The file created will include the current UTC offset in minutes as a 2-byte signed integer. The device will use this offset, along with the timestamp of the file, to set its own system clock. 
Because the system time is dependent on the file timestamp, the HPR drive should be ejected as soon after the file is written as possible.

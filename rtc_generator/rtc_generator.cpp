/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       rtc_generator.cpp
 *          - This program generates a *.RTC timestamp file to update the timestamp on HPR devices
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./rtc_generator utc_offset > output.rtc
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cerrno>

#ifdef WIN32
#include <io.h>
#include <fcntl.h>
#endif

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Public Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in an SCP-ECG file from inFile and parses out all of
//    the header information, ignoring the actual ECG data.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  int64_t offset;

#ifdef WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif

  // If there were no command line arguments, just display the usage info
  if(argc < 2)
  {
    string programName(argv[0]);
    cout << "Usage: " << programName << " [-780:780,32767]" << endl;

    return 1;
  }

  // This uses a string and a stringstream to read a number from the first
  // command-line argument.
  string offset_str(argv[1]);
  stringstream offset_ss(offset_str, ios_base::in);
  offset_ss >> offset;

  // If the beginning of the argument is not a number, or there are non-digit
  // characters at the end of the argument, print an error
  if (offset_ss.fail() || !offset_ss.eof())
  {
    cerr << "Input Error: \"" << offset_str << "\" is not a number. Aborting." << endl;
    return -1; 
  }
  // Negative offsets less than -780 aren't possible (minus 13 hours)
  else if (offset < -780)
  {
    cerr << "Input Error: \"" << offset << "\" is less than the minimum value (-780). Aborting." << endl;
    return -1;
  }
  // Positive offsets greater than 780 aren't allowed (plus 13 hours), UNLESS
  // it is the special case of 32767 which indicates "Unkown timezone"
  else if ( (offset > 780) && (32767 != offset) )
  {
    cerr << "Input Error: \"" << offset << "\" is greater than the maximum value (780). Aborting." << endl;
    return -1;
  }
  // Offset values that are not a multiple of 15 aren't allowed by the SC Spec
  else if ( (offset % 15) && (32767 != offset) )
  {
    cerr << "Input Error: \"" << offset << "\" is not a multiple of 15. Aborting." << endl;
    return -1;
  }

  // Write the file out!
  char offset_bytes[2];

  offset_bytes[0] = (offset & 0x00ff);
  offset_bytes[1] = ((offset >> 8) & 0x00ff);

  cout.write((const char *) offset_bytes, 2);  

  return 0;
}



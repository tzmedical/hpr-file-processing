# HPR EVENTS SPECIFICATION #

## Overview ##

The HPR device saves individual event files (\*.tze) for each event recorded by the device. The format of the files is below, along with the possible event tags.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZEVT" to identify the file as an event file |
| Device String | 6 | The ASCII string "HPR" to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| Patient ID | 40 | ASCII String identifier unique to patient |
| Event Tag | 1 | This value identifies the event associated with the data to follow. Possible values are described below. |
| Sequence Number | 4 | This is the ECG sequence number of the file associated with this event, corresponding to tag 31 on page 34 of the SCP-ECG Standard version 2.2 |
| Sample Count | 2 | Number of samples since beginning of SCP file |
| Date | 4 | This is the date of the entry following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Time | 3 | This is the time of the entry (using UTC), following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Milliseconds | 1 | This is the number of 4 millisecond time units to pass since the last second change, to provide better distinction between events |
| Time Zone | 2 | This is the number of minutes that local time is offset from UTC. Valid range is -780 to 780 |
| Data Length | 1 | This is the number of bytes of data associated with this event |
| Data | <_length_> | The value of this section is dependent on the value of the tag. Refer to the events README |
| File Count | 1 | This is the number of SCP-ECG files associated with this event |
| Start File | 4 | This is the sequence number of the first SCP-ECG file associated with this event |
| Reserved | 1 | Null byte. This is a '\0' used to identify the end of each entry |

## Event Tag Tool Tips ##

### Recording Started ###

Event Tag: **1**.

Event Data: **File identifier**. Pulled from the most current setting file.

This event indicates when a new recording is started using the device's User Interface.

### Recording Suspended ###

Event Tag: **3**.

Event Data: **100**. Unknown suspending source.

This event indicates when the recording has been interrupted due to a low battery condition or the patient cable being disconnected.

### Recording Resumed ###

Event Tag: **4**.

Event Data: **File identifier**. Pulled from the most current setting file.

This event indicates when the recording is resumed following an interruption due to a low battery condition or a patient cable disconnect.

### Recording Full ###

Event Tag: **5**.

Event Data: **11**. Preset length reached.

This event indicates when the recording has been stopped due to reaching *EITHER* the predefined stopping point OR the maximum capacity of the SD Card, whichever comes first. 

### Pacemaker Detection ###

Event Tag: **11**.

Event Data: **Width of the detected pulse**. Measured in microseconds.

This event indicates that the associated SCP-ECG file contains at least one detected pacemaker pulse. 

### End of SCP File ###

Event Tag: **16**.

Event Data: **File identifier**. Pulled from the most current setting file.

This event indicates the number of samples in a given SCP file to facilitate R-R Interval calculations.

### Lead Disconnected ###

Event Tag: **51**.

Event Data: **A binary mask of the electrode connections**. When set to 1, each bit indicates that the corresponding lead is off.

This event indicates that one or more snap electrodes have become disconnected. 

### Patient Activated Event ###

Event Tag: **101**.

Event Data: **0xTTSS**, Where **SS** is the selected symptom number (0 == none) and **TT** is the selected activity level number (0 == none).

This event indicates that the patient has felt something and held the patient event button on the device. 

### Step Count ###

Event Tag: **102**.

Event Data: **Number of steps**. The number of steps since the last Step Count event.

This event records the number of steps that the device detected since the last Step Count event.

### Battery State of Charge ###

Event Tag: **150**.

Event Data: **Percent charge** Recorded multiplied by 256. (e.g. 100% -> 25600).

This event indicates the current percent charge of the battery.  

### Battery Voltage ###

Event Tag: **151**.

Event Data: **Battery voltage**. Measured in mV.

This event indicates the current voltage of the battery.

### Low Battery ###

Event Tag: **152**.

Event Data: **Battery voltage**. Measured in mV..

This event indicates that the battery is too low for continued operation.

### Charging Started ###

Event Tag: **153**.

Event Data: **None**.

This event indicates that the battery charger has been connected to the device.

### Charging Stopped ###

Event Tag: **154**.

Event Data: **None**.

This event indicates that the battery charger has been disconnected from the device.

### Terminator ###

Event Tag: **255**.

Event Data: **255** Equal to Event Tag.

This event indicates the end of a TZR file.
# Deprecation Notice #

This code is no longer maintained. Support for new features will NOT be added to this repository. 

We strongly recommend switching over to the actively maintained [trident-sdk](https://bitbucket.org/tzmedical/trident-sdk/src/master/) repository. 

[Trident-sdk](https://bitbucket.org/tzmedical/trident-sdk/src/master/) is compatible with and supports all Trident cardiac monitoring devices.

If you have any questions feel free to contact us!

Thank you for working with us through this transition!
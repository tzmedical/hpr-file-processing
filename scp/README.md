# ECG DATA Format #

## Overview ##

Recorded ECG data will be stored in compliance with the SCP-ECG Standard (ISO/DIS 11073-91064). A copy of the SCP-ECG Standard (Version 2.2) is included along with this document. As SCP-ECG was created to be a flexible file format for a number of ECG applications, the spcifics of this particular implementation are listed below:

| Compliance Category |  |
| --- | --- |
| Data Compression | *None* |
| Data Encoding | *Huffman Encoding and Difference Encoding* |
| Data Sections Included | *0, 1, 2, 3, 6, 7* |
| File Length | *Variable, based on resolution and sample rate* |
| File Size | 32 Kbytes |

### SCP-ECG Sections ###

The HPR will use the SCP-ECG file sections listed above according to the following table:

| SCP-ECG File Section | Usage | 
| --- | --- |
| Section 0 | This is the Index section. It will be present in every SCP-ECG file and may be used to identify which other sections are present in the file |
| Section 1 | This section is used to identify the device used to record this file. It includes the 40-character “Patient ID” field, the HPR serial number, the frequency response of the analog circuit, and the sequential number of this recording |
| Section 2 | This section contains Huffman tables to be used for encoding the data stored in section 6 |
| Section 3 | This section is used to identify the lead configuration employed during this recording, if it has been specified in the HPR settings file. It also identifies how many samples are present for each lead |
| Section 6 | This is the ECG Data section and contains the data recorded by the HPR. Based on contents of section 2, this will contain the data as Huffman encoded values. This data will be stored as difference-encoded values.
| Section 7 | This is the “global measurements” section of the SCP-ECG file. For the HPR it will only be populated with data if the HPR has detected at least one pacemaker spike during the recording interval |

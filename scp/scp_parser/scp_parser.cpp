/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scp_parser.cpp
 *          - This program parses an *.scp ECG file from <input.scp> and outputs a
 *            text interpretation of the header information on <cout>, ignoring
 *            the actual ECG data.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./scp_parser input.scp > output.txt
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC                   // Comment this line to disable CRC Checking
//#define PACEMAKER_EXTENDED_INFO     // Comment this line to shorten pacemaker outputs

#define SCP_ID_STRING      "SCPECG"

#define MIN_FILE_SIZE      (256)
#define MAX_FILE_SIZE      (256*1024)
#define DEFAULT_CODE_COUNT    19

static const int default_prefix[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26,
};
static const int default_total[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10,
};
static const int default_value[DEFAULT_CODE_COUNT] = {
  0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0,
};
static const int default_code[DEFAULT_CODE_COUNT] = {
  0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023,
};


///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  if(!length){
    cout << "Attempting to CRC 0 bytes! Aborting." << endl;
    return -1;
  }
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}

unsigned char read8(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return *pC;
}

unsigned short read16(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return (unsigned short) pC[0] + (unsigned short) pC[1]*256;
}

unsigned int read32(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return (((unsigned int)pC[3]*256 + (unsigned int)pC[2])*256 
      + (unsigned int)pC[1])*256 + (unsigned int) pC[0];
}

int sectionHeader(unsigned char *pData, unsigned int *length){
  unsigned short theircrcValue = read16(pData, 0);                // CRC for section
  unsigned int id = read16(pData, 2);                             // Section ID for Section
  *length = read32(pData, 4);                                     // Length for Section
  unsigned int sVersion = read8(pData, 8);                        // Section Version (2.2)
  unsigned int pVersion = read8(pData, 9);                        // Protocol Version (2.2)

  cout << "\n****** Section " << id << " Header ******" << endl;

  unsigned short ourcrcValue = 0xffff;
  crcBlock(&pData[2], (*length)-2, &ourcrcValue);

#ifdef CHECK_CRC
  if(ourcrcValue != theircrcValue){
    cout << "CRC error! file: 0x" << hex << theircrcValue
      << " - calculated: 0x" << ourcrcValue << dec << endl;
    cout << "Aborting." << endl;
    return -1;
  }
  else{
    cout << "CRC Valid: 0x" << hex << theircrcValue << dec << endl;
#else
    cout << "Ignoring CRC (0x" << hex << theircrcValue 
      << " : 0x" << ourcrcValue << ")" << dec << endl;
    {
#endif
      cout << "Section Length: " << *length << endl;
      cout << "Section Version: " << sVersion/10 << "." << sVersion%10 << endl;
      cout << "Protocol Version: " << pVersion/10 << "." << pVersion%10 << endl;
    }

    return 0;
  }

  class subSection
  {
    private:
      unsigned short id;
      unsigned int length;
      unsigned int index;
      unsigned char *pData;

    public:
      subSection(){
        id = 0;
        length = 0;
        index = 0;
        pData = new unsigned char [2];
      }

      ~subSection(){
        delete [] pData;
      }

      void init(unsigned short i, unsigned int len,
          unsigned int ind, unsigned char *pD){
        id = i;
        length = len;
        index = ind;
        delete [] pData;
        pData = new unsigned char [length];
        unsigned int j;
        for(j = 0; j < length; j++){
          pData[j] = pD[j + index - 1];
        }
      }

      unsigned read_8(unsigned int *offset){
        if(*offset > (length-1)) {
          cerr << "Section Read Overflow!" << endl;
          return 0;
        }
        *offset += 1;
        return read8(pData, *offset-1);
      }

      unsigned read_16(unsigned int *offset){
        if(*offset > (length-2)) {
          cerr << "Section Read Overflow!" << endl;
          return 0;
        }
        *offset += 2;
        return read16(pData, *offset-2);
      }

      unsigned read_32(unsigned int *offset){
        if(*offset > (length-4)) {
          cerr << "Section Read Overflow!" << endl;
          return 0;
        }
        *offset += 4;
        return read32(pData, *offset-4);
      }

      void readString(unsigned int *offset, string *str, unsigned int len){
        if(*offset > (length-len)){
          cerr << "Section Read Overflow!" << endl;
          return;
        }
        str->assign((const char*)&(pData[*offset]),(int) len);
        *offset += len;
      }


      int readHeader(unsigned int *length){
        return sectionHeader(pData, length);
      }

      bool exists(){
        //cout << "exist check on #" << id << " - Length: " << length << " - Index: " << index << endl;
        return length?1:0;
      }
  };



  ///////////////////////////////////////////////////////////////////////////////
  //       Public Functions
  ///////////////////////////////////////////////////////////////////////////////


  //-----------------------------------------------------------------------------
  //    main()
  //
  //    This function reads in an SCP-ECG file from inFile and parses out all of
  //    the header information, ignoring the actual ECG data.
  //-----------------------------------------------------------------------------
  int main(int argc, char *argv[])
  {
    unsigned short calculatedCrcValue;                  // The CRC we calculate
    unsigned short theircrcValue;                // The CRC from the file
    unsigned int length;                         // The length read from the file

    unsigned char * pRead = NULL;                // Pointer used for the read command

    unsigned char firstBlock[32];

    unsigned int i;



    if(argc < 2){
      cerr << "No input file specified! Aborting." << endl;
      return -1;
    }

    // Open the file and check the file length
    fstream inFile (argv[1], ios::in | ios::binary);
    inFile.seekg(0, ios::end);
    unsigned int fileLength = inFile.tellg();
    inFile.seekg(0, ios::beg);

    // Make sure the file is not too big for the format
    if(fileLength > MAX_FILE_SIZE){
      cerr << "File size is SIGNIFICANTLY larger than expected. Aborting." << endl;
      return -1;
    }

    // Make sure the file is not too small for the format
    if(fileLength < MIN_FILE_SIZE){
      cerr << "File size is too small for an SCP file. Aborting." << endl;
      return -1;
    }

    // Read in the first TWO 16 byte blocks to see if the file has been encrypted
    for(i = 0; i < 32; i++){
      firstBlock[i] = 0;
    }
    inFile.read((char *) firstBlock, 32);           // Read the first block into RAM

    string str;                                  // Check that we are reading an SCP file
    str.assign((const char*)&firstBlock[16], 6);           // Copy what should be the "SCPECG" string
    if(str.compare(0, 6, SCP_ID_STRING)){
      cerr << "ERROR: File corrupted! Aborting." << endl;
      return -1;
    }
    else
    {
      theircrcValue = read16(firstBlock, 0);
      length = read32(firstBlock, 2);

      pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
      for(i = 0; i < 32; i++){
        pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
      }
      inFile.read((char *) &pRead[32], fileLength-32);    // Store the remainder of the file in memory

      calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
      crcBlock(&pRead[2], length-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file
    }

#ifdef CHECK_CRC
    if(calculatedCrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
      cout << "File CRC Valid: 0x" << hex << theircrcValue << dec << endl;  // Notify user that we are parsing
#else
      cout << "Ignoring CRC (0x" << hex << theircrcValue 
        << " : 0x" << calculatedCrcValue << ")" << dec << endl;  // Ignore the CRC values (debugging ONLY)
#endif

      cout << "File Length: " << length << endl;            // Print out the file length    

      unsigned int sI = 6;
      unsigned int j;

      /**********************************************************************************************
       *          Section 0 
       **********************************************************************************************/
      if(sectionHeader(&pRead[sI], &length)){               // Parse the header for section 0
        delete[] pRead;                                    // Return value -> CRC Error
        return -1;
      }

      subSection sections[12];                              // Array of sections to store file data

      for(j = 16; j < length; j += 10){
        unsigned short id = read16(&pRead[sI], j);         // Section Number: 0 - 11
        unsigned int len = read32(&pRead[sI], j+2);        // Section Length
        unsigned int ind = read32(&pRead[sI], j+6);        // Section Start Index
        cout << "Section: " << id
          << " - Length: " << len
          << " - Index: " << ind << endl;
        if(id < 12){
          sections[id].init(id, len, ind, pRead);   // Copy data into section classes for later access
        }
        else{                                        // We've hit some sort of error
          cerr << "Unexpected section index(" << id << ")! Aborting." << endl;
          delete[] pRead;
          return -1;
        }
      }

      delete[] pRead;                 // Free the buffer we used for file input

      /**********************************************************************************************
       *          Section 1 
       **********************************************************************************************/

      if(sections[1].exists()){
        if(sections[1].readHeader(&length)) return -1;
        for(j = 16; j < length; ){
          unsigned char tag = sections[1].read_8(&j);
          unsigned short tagLength = sections[1].read_16(&j);
          cout << "(tag=" << (int) tag << ") (length=" << (int) tagLength << ") ";
          switch(tag){
            case 0: {
                      string name;
                      sections[1].readString(&j, &name, tagLength);
                      cout << "[TAG] Last Name: " << name << endl;
                    }break;
            case 1: {
                      string name;
                      sections[1].readString(&j, &name, tagLength);
                      cout << "[TAG] First Name: " << name << endl;
                    }break;
            case 2: {                // Patient ID
                      string patientID;
                      sections[1].readString(&j, &patientID, tagLength);
                      cout << "[TAG] Patient ID: " << patientID << endl;
                    }break;
            case 3: {
                      string name;
                      sections[1].readString(&j, &name, tagLength);
                      cout << "[TAG] SecondLast Name: " << name << endl;
                    }break;

            case 14: {              // Device ID
                       unsigned int institution = sections[1].read_16(&j);      // Not sure what this is, set to 0
                       unsigned int department = sections[1].read_16(&j);       // Not sure what this is, set to 0
                       unsigned int deviceID = sections[1].read_16(&j);         // Not sure what this is, set to 0
                       bool deviceType = sections[1].read_8(&j);                // 0 = cart, 1 = Host
                       unsigned int mfgCode = sections[1].read_8(&j);           // 255 = check string at end
                       string modelDesc;
                       sections[1].readString(&j, &modelDesc, 6);               // ASCII description of device
                       unsigned int protocolRev = sections[1].read_8(&j);       // SCP protocol version
                       unsigned int protocolCompat = sections[1].read_8(&j);    // Category I or II
                       unsigned int languageSupport = sections[1].read_8(&j);   // 0 = ASCII
                       unsigned int deviceCapability = sections[1].read_8(&j);  // print, interpret, store, acquire
                       unsigned int mainsFreq = sections[1].read_8(&j);         // 50 Hz, 60 Hz, Unspecified
                       j += 16;
                       unsigned int apRevLength = sections[1].read_8(&j);       // Length of Revision String
                       string apRev;
                       sections[1].readString(&j, &apRev, apRevLength);         // ASCII Revision of analysis prog.
                       unsigned int devSerNoLength = sections[1].read_8(&j);    // Length of Serial Number String
                       string devSerNo;
                       sections[1].readString(&j, &devSerNo, devSerNoLength);   // ASCII Device Serial Number
                       unsigned int devSoftNoLength = sections[1].read_8(&j);   // Length of software string
                       string devSoftNo;
                       sections[1].readString(&j, &devSoftNo, devSoftNoLength); // ASCII Software Version
                       unsigned int devSCPidLength = sections[1].read_8(&j);    // Length of SCP software ID
                       string devSCPid;
                       sections[1].readString(&j, &devSCPid, devSCPidLength);   // ASCII SCP software ID
                       unsigned int mfgStringLength = sections[1].read_8(&j);   // Length of mfg name
                       string mfgString;
                       sections[1].readString(&j, &mfgString, mfgStringLength); // Manufacturer Name
                       cout << "[TAG] Device ID:" << endl;
                       cout << "---Institution Number: " << institution << endl;
                       cout << "---Department Number: " << department << endl;
                       cout << "---Device ID: " << deviceID << endl;
                       cout << "---Device Type: " << deviceType << (deviceType?" (Host)":" (Cart)") << endl;
                       cout << "---Manufacturer Number: " << mfgCode << ((mfgCode==255)?" (See string)":"") << endl;
                       cout << "---Model Description: " << modelDesc << endl;
                       cout << "---SCP-ECG Version: " << (protocolRev/10) << "." << (protocolRev%10) << endl;
                       cout << "---SCP-ECG Compatibility: 0x" << hex << protocolCompat << dec;
                       if( (protocolCompat&0xf0) == 0xd0) cout << " (Category I)" << endl;
                       else if( (protocolCompat&0xf0) == 0xe0) cout << " (Category II)" << endl;
                       else cout << " (unknown)" << endl;
                       cout << "---Language Support: 0x" << hex << languageSupport << dec
                         << (languageSupport?" (unknown)":" (ASCII Only)") << endl;
                       cout << "---Device Capabilities: 0x" << hex << deviceCapability << dec << endl;
                       cout << "------Print: " << ((deviceCapability&0x10)?"yes":"no") << endl;
                       cout << "------Interpret: " << ((deviceCapability&0x20)?"yes":"no") << endl;
                       cout << "------Store: " << ((deviceCapability&0x40)?"yes":"no") << endl;
                       cout << "------Acquire: " << ((deviceCapability&0x80)?"yes":"no") << endl;
                       cout << "---Mains Frequency Environment: " << mainsFreq;
                       if(mainsFreq == 0) cout << " (Unspecified)" << endl;
                       else if(mainsFreq == 1) cout << " (50 Hz)" << endl;
                       else if(mainsFreq == 2) cout << " (60 Hz)" << endl;
                       else cout << " (ERROR)" << endl;
                       cout << "---Analysis Program Revision:  (" << apRevLength << ")" << apRev << endl;
                       cout << "---Device Serial Number: (" << devSerNoLength << ") " << devSerNo << endl;
                       cout << "---Device Software Number:  (" << devSoftNoLength << ")" << devSoftNo << endl;
                       cout << "---Device SCP Software:  (" << devSCPidLength << ")" << devSCPid << endl;
                       cout << "---Device Manufacturer (" << mfgStringLength << "): " << mfgString << endl;
                     }break;
            case 25: {              // Date of Acquisition
                       if(tagLength != 4){
                         cerr << "Error Parsing Date! Length = " << (int) tagLength << endl;
                         return -1;
                       }
                       else{
                         unsigned short year = sections[1].read_16(&j);
                         unsigned char month = sections[1].read_8(&j);
                         unsigned char day = sections[1].read_8(&j);
                         cout << "[TAG] Date: " << setw(4) << setfill('0') << (int) year
                           << "/" << setw(2) << (int) month
                           << "/" << setw(2) << (int) day << setfill(' ') << endl;
                       }
                     }break;
            case 26: {              // Time of Acquisition
                       if(tagLength != 3){
                         cerr << "Error Parsing Time! Length = " << tagLength << endl;
                         return -1;
                       }
                       else{
                         unsigned char hour = sections[1].read_8(&j);
                         unsigned char minutes = sections[1].read_8(&j);
                         unsigned char seconds = sections[1].read_8(&j);
                         cout << "[TAG] Time: " << setw(2) << setfill('0') << (int) hour
                           << ":" << setw(2) << (int) minutes
                           << ":" << setw(2) << (int) seconds << setfill(' ') << endl;
                       }
                     }break;
            case 27: {              // High Pass Filter
                       if(tagLength != 2){
                         cerr << "Error Parsing HP Filter! Length = " << (int) tagLength << endl;
                         return -1;
                       }
                       else{
                         unsigned short hpFilter = sections[1].read_16(&j);
                         cout << "[TAG] High Pass Filter: " << (int) hpFilter/100 
                           << "." << setw(2) << setfill('0') << (int) hpFilter%100 
                           << " Hz" << setfill(' ') << endl;
                       }
                     }break;
            case 28: {              // Low Pass Filter
                       if(tagLength != 2){
                         cerr << "Error Parsing LP Filter! Length = " << (int) tagLength << endl;
                         return -1;
                       }
                       else{
                         unsigned short lpFilter = sections[1].read_16(&j);
                         cout << "[TAG] Low Pass Filter: " << (int) lpFilter << " Hz" << endl;
                       }
                     }break;
            case 31:{
                      string ecgSequence;
                      sections[1].readString(&j, &ecgSequence, tagLength);
                      cout << "[TAG] ECG Sequence Number: " << ecgSequence << endl;
                    }break;
            case 34:{               // Time Zone
                      signed short offset = sections[1].read_16(&j);
                      unsigned short index = sections[1].read_16(&j);
                      string desc;
                      sections[1].readString(&j, &desc, tagLength - 4);
                      cout << "[TAG] Date Time Zone:" << endl;
                      cout << "---Minutes Offset from UTC: " << offset << endl;
                      cout << "---Time Zone Index (unused): " << index << endl;
                      cout << "---Time Zone Description (unused): \"" << desc << "\"" << endl;
                    }break;
            case 255: {             // Terminator - we're done.
                        if(tagLength != 0){
                          cerr << "Error Parsing Terminator! Length = " << (int) tagLength << endl;
                        }
                        else{
                          cout << "[TAG] Terminator." << endl;
                          j = length;
                        }
                      }break;
            default: {
                       cout << "[TAG] " << (int) tag << " (unsupported)" << endl;
                       j += tagLength;
                     }break;
          }
        }

      }


      /**********************************************************************************************
       *          Section 2 
       **********************************************************************************************/

      unsigned int **prefixBits = NULL;
      unsigned int **totalBits = NULL;
      unsigned int **switchByte = NULL;
      unsigned int **baseValue = NULL;
      unsigned int **baseCode = NULL;
      unsigned int tableCount = 0;
      unsigned int *codeCount = NULL;
      if(sections[2].exists()){
        if(sections[2].readHeader(&length)) return -1;
        j = 16;
        tableCount = sections[2].read_16(&j);
        unsigned int l;
        if(19999 != tableCount){
          cout << "Number of Huffman Tables: " << tableCount << endl;
          prefixBits = new unsigned int *[tableCount];
          totalBits = new unsigned int *[tableCount];
          switchByte = new unsigned int *[tableCount];
          baseValue = new unsigned int *[tableCount];
          baseCode = new unsigned int *[tableCount];

          codeCount = new unsigned int [tableCount];

          for(l = 0; l < tableCount; l++){
            codeCount[l] = sections[2].read_16(&j);
            cout << "Number of codes in Table #" << (l+1) << ": " << codeCount[l] << endl;
            prefixBits[l] = new unsigned int [codeCount[l]];
            totalBits[l] = new unsigned int [codeCount[l]];
            switchByte[l] = new unsigned int [codeCount[l]];
            baseValue[l] = new unsigned int [codeCount[l]];
            baseCode[l] = new unsigned int [codeCount[l]];
            unsigned int m;
            for(m = 0; m < codeCount[l]; m++){
              prefixBits[l][m] = sections[2].read_8(&j);
              totalBits[l][m] = sections[2].read_8(&j);
              switchByte[l][m] = sections[2].read_8(&j);
              baseValue[l][m] = sections[2].read_16(&j);
              baseCode[l][m] = sections[2].read_32(&j);

              cout << "---Code #" << (m+1) << " details:" << endl;
              cout << "------Prefix Bits: " << prefixBits[l][m] << endl;
              cout << "------Total Bits: " << totalBits[l][m] << endl;
              cout << "------Switch Byte: " << switchByte[l][m] << endl;
              cout << "------Base Value: " << baseValue[l][m] << endl;
              cout << "------Base Code: 0b";// << baseCode[m] << " (0b";

              unsigned int n;
              unsigned int bitmask = 1;
              for(n = 0; n < prefixBits[l][m]; n++){
                cout << ((baseCode[l][m] & bitmask)?1:0);
                bitmask = bitmask<<1;
              }
              cout << " (" << baseCode[l][m] << ")" << endl;
            }

          }
        }
        else{
          cout << "Default Huffman Table used!" << endl;
          prefixBits = new unsigned int *[1];
          totalBits = new unsigned int *[1];
          switchByte = new unsigned int *[1];
          baseValue = new unsigned int *[1];
          baseCode = new unsigned int *[1];

          codeCount = new unsigned int [1];


          codeCount[0] = DEFAULT_CODE_COUNT;
          prefixBits[0] = new unsigned int [codeCount[0]];
          totalBits[0] = new unsigned int [codeCount[0]];
          switchByte[0] = new unsigned int [codeCount[0]];
          baseValue[0] = new unsigned int [codeCount[0]];
          baseCode[0] = new unsigned int [codeCount[0]];

          unsigned int m;
          for(m = 0; m < codeCount[0]; m++){
            prefixBits[0][m] = default_prefix[m];
            totalBits[0][m] = default_total[m];
            switchByte[0][m] = 1;
            baseValue[0][m] = default_value[m];
            baseCode[0][m] = default_code[m];

            cout << "---Code #" << (m+1) << " details:" << endl;
            cout << "------Prefix Bits: " << prefixBits[0][m] << endl;
            cout << "------Total Bits: " << totalBits[0][m] << endl;
            cout << "------Switch Byte: " << switchByte[0][m] << endl;
            cout << "------Base Value: " << baseValue[0][m] << endl;
            cout << "------Base Code: 0b";// << baseCode[m] << " (0b";

            unsigned int n;
            unsigned int bitmask = 1;
            for(n = 0; n < prefixBits[0][m]; n++){
              cout << ((baseCode[0][m] & bitmask)?1:0);
              bitmask = bitmask<<1;
            }
            cout << " (" << baseCode[0][m] << ")" << endl;
          }
        }
      }


      /**********************************************************************************************
       *          Section 3 
       **********************************************************************************************/

      unsigned int leadCount = 0;
      unsigned int *sampleStart = NULL, *sampleEnd = NULL, *leadID = NULL;
      i = 0;
      if(sections[3].exists()){
        if(sections[3].readHeader(&length)) return -1;
        j = 16;
        leadCount = sections[3].read_8(&j);
        unsigned int flags = sections[3].read_8(&j);
        sampleStart = new unsigned int [leadCount];
        sampleEnd = new unsigned int [leadCount];
        leadID = new unsigned int [leadCount];
        for(i = 0; i < leadCount; i++){
          sampleStart[i] = sections[3].read_32(&j);
          sampleEnd[i] = sections[3].read_32(&j);
          leadID[i] = sections[3].read_8(&j);
        }
        cout << "Number of Leads: " << leadCount << endl;
        cout << "Flags: 0x" << hex << flags << dec << endl;
        cout << ((flags&0x01)?"---Reference beat subtraction used for compression":
            "---Reference beat subtraction not used for compression") << endl;
        cout << ((flags&0x04)?"---Leads recorded simultaneously":
            "---Leads not recorded simultaneously") << endl;
        cout << "---Number of simultaneously recorded leads: " << (flags/8) << endl;
        for(i = 0; i < leadCount; i++){
          cout << "Details for Lead #" << i << endl;
          cout << "---Starting Sample Number: " << sampleStart[i] << endl;
          cout << "---Ending Sample Number: " << sampleEnd[i] << endl;
          cout << "---Lead Identification Code: " << leadID[i];
          if(leadID[i] == 131) cout << " (ES)" << endl;
          else if(leadID[i] == 132) cout << " (AS)" << endl;
          else if(leadID[i] == 133) cout << " (AI)" << endl;
          else cout << endl;
        }

      }


      /**********************************************************************************************
       *          Section 6 
       **********************************************************************************************/

      unsigned int *leadLength = NULL;
      unsigned int **dataArrays = NULL;
      if(sections[6].exists()){
        if(sections[6].readHeader(&length)) return -1;
        j = 16;
        unsigned int ampMult = sections[6].read_16(&j);
        unsigned int samplePeriod = sections[6].read_16(&j);
        unsigned int encoding = sections[6].read_8(&j);
        unsigned int compression = sections[6].read_8(&j);
        leadLength = new unsigned int [leadCount];
        dataArrays = new unsigned int *[leadCount];
        for(i = 0; i < leadCount; i++){
          leadLength[i] = sections[6].read_16(&j);
          if(sampleEnd[i] > sampleStart[i])
          {
            dataArrays[i] = new unsigned int [sampleEnd[i] - sampleStart[i]];
          }
          else dataArrays[i] = NULL;
        }
        cout << "Amplitude multiplier: " << ampMult << " (nV/count)" << endl;
        if(ampMult){
          cout << "---ADC Gain: " << (1000000./(float)ampMult) << " (counts/mV)" << endl;
        }
        cout << "Sample Period: " << samplePeriod << " (us)" << endl;
        if(samplePeriod){
          cout << "---Sample Rate: " << (1000000/samplePeriod) << " (Hz)" << endl;
        }
        cout << "Data encoding: " << encoding << endl;
        if(encoding == 0) cout << "---Real data" << endl;
        else if(encoding == 1) cout << "---First difference data" << endl;
        else if(encoding == 2) cout << "---Second difference data" << endl;
        else cout << "---Unknown data" << endl;
        cout << "Compression: " << compression
          << (compression?" (Bimodal compression)":" (No compression)") << endl;
        for(i = 0; i < leadCount; i++){
          cout << "Bytes of data for lead #" << i << ": " << leadLength[i] << endl;
        }
      }

      /**********************************************************************************************
       *          Section 7 
       **********************************************************************************************/


      unsigned int *paceTime, *paceAmplitude;
      unsigned int *paceType, *paceSource, *paceIndex, *paceWidth;
      if(sections[7].exists()){
        if(sections[7].readHeader(&length)) return -1;
        j = 16;
        unsigned int referenceCount = sections[7].read_8(&j);
        unsigned int paceCount = sections[7].read_8(&j);
        unsigned int rrInterval = sections[7].read_16(&j);
        unsigned int ppInterval = sections[7].read_16(&j);
        paceTime = new unsigned int [paceCount];
        paceAmplitude = new unsigned int [paceCount];
        for(i = 0; i < paceCount; i++){
          paceTime[i] = sections[7].read_16(&j);
          paceAmplitude[i] = sections[7].read_16(&j);
        }
        paceType = new unsigned int [paceCount];
        paceSource = new unsigned int [paceCount];
        paceIndex = new unsigned int [paceCount];
        paceWidth = new unsigned int [paceCount];
        for(i = 0; i < paceCount; i++){
          paceType[i] = sections[7].read_8(&j);
          paceSource[i] = sections[7].read_8(&j);
          paceIndex[i] = sections[7].read_16(&j);
          paceWidth[i] = sections[7].read_16(&j);
        }
        cout << "Number of reference beat types: " << referenceCount << endl;;
        cout << "Number of pacemaker spikes: " << paceCount << endl;
        cout << "Average RR interval: ";
        if(rrInterval == 29999) cout << "not calculated" << endl;
        else cout << rrInterval << " (ms)" << endl;
        cout << "Average PP interval: ";
        if(ppInterval == 29999) cout << "not calculated" << endl;
        else cout << ppInterval << " (ms)" << endl;
        for(i = 0; i < paceCount; i++){
          cout << "Details for pacemaker spike #" << i+1 << endl;
#ifdef PACEMAKER_EXTENDED_INFO
          cout << "(" << paceTime[i] << " " << paceAmplitude[i] << " "
            << paceType[i] << " " << paceSource[i] << " "
            << paceIndex[i] << " " << paceWidth[i] << ")" << endl;
          cout << "---Time: " << paceTime[i] << " (ms)" << endl;
          cout << "---Amplitude: ";
          if(paceAmplitude[i] == 29999) cout << "not calculated" << endl;
          else cout << paceAmplitude[i] << " (uV)" << endl;
          cout << "---Type: ";
          if(paceType[i] == 255) cout << "not calculated" << endl;
          else if(paceType[i] == 0) cout << "unknown" << endl;
          else if(paceType[i] == 1) cout << "triggers neither P-wave nor QRS" << endl;
          else if(paceType[i] == 2) cout << "triggers a QRS" << endl;
          else if(paceType[i] == 3) cout << "triggers a P-wave" << endl;
          else cout << "ERROR" << endl;
          cout << "---Source: ";
          if(paceSource[i] == 0) cout << "unknown" << endl;
          else if(paceSource[i] == 1) cout << "internal" << endl;
          else if(paceSource[i] == 2) cout << "external" << endl;
          else cout << "ERROR" << endl;
          cout << "---Triggered QRS complex: ";
          if(paceIndex[i] == 0) cout << "none" << endl;
          else cout << paceIndex[i];
          cout << "---Pulse Width: ";
          if(paceWidth[i] == 0) cout << "unknown" << endl;
          else cout << paceWidth[i] << " (us)" << endl;
#else
          cout << "---Pulse Width: ";
          if(paceWidth[i] == 0) cout << "unknown";
          else cout << paceWidth[i] << " (us)";
          cout << " at time: " << paceTime[i] << " (ms)" << endl;
#endif
        }
      }


      if(sections[2].exists()){
        unsigned int l;
        for(l = 0; l < tableCount; l++){
          delete[] prefixBits[l];
          delete[] totalBits[l];
          delete[] switchByte[l];
          delete[] baseValue[l];
          delete[] baseCode[l];
        }
        delete[] codeCount;
        delete[] prefixBits;
        delete[] totalBits;
        delete[] switchByte;
        delete[] baseValue;
        delete[] baseCode;
      }

      if(sections[3].exists()){
        delete[] sampleStart;
        delete[] sampleEnd;
        delete[] leadID;
      }

      if(sections[6].exists()){
        delete[] leadLength;
        unsigned int l;
        for(l = 0; l < leadCount; l++){
          delete[] dataArrays[l];
        }
        delete[] dataArrays;
      }

      if(sections[7].exists()){
        delete[] paceTime;
        delete[] paceAmplitude;
        delete[] paceType;
        delete[] paceSource;
        delete[] paceIndex;
        delete[] paceWidth;
      }


#ifdef CHECK_CRC
    }
    else{
      cerr << "File CRC Invalid. Halting\n";
      cerr << "thier CRC: " << theircrcValue << " - our CRC: " << calculatedCrcValue << endl;
      cerr << "File Length: " << length << endl;            // Print out the file length      
    }
#endif

    cout << "Done.\n" << endl;                 // Signal completion of program

    return 0;
  }



/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       ecg_splitter.cpp
 *          - This program parses an *.bin ECG file from <input.scp> and outputs
 *              all the individual files
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./ecg_splitter input.bin
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include<sys/stat.h>

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC                   // Comment this line to disable CRC Checking
//#define PACEMAKER_EXTENDED_INFO     // Comment this line to shorten pacemaker outputs

#define SCP_ID_STRING      "SCPECG"

#define MIN_FILE_SIZE      (256)
#define MAX_FILE_SIZE      (256*1024)
#define DEFAULT_CODE_COUNT    19

#define SCP_SIZE (32766)

static const int default_prefix[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26,
};
static const int default_total[DEFAULT_CODE_COUNT] = {
  1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10,
};
static const int default_value[DEFAULT_CODE_COUNT] = {
  0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0,
};
static const int default_code[DEFAULT_CODE_COUNT] = {
  0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023,
};


///////////////////////////////////////////////////////////////////////////////
//       Constructor
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  if(!length){
    cout << "Attempting to CRC 0 bytes! Aborting." << endl;
    return -1;
  }
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}

unsigned char read8(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return *pC;
}

unsigned short read16(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return (unsigned short) pC[0] + (unsigned short) pC[1]*256;
}

unsigned int read32(unsigned char *pData, unsigned int offset)
{
  unsigned char *pC = &pData[offset];
  return (((unsigned int)pC[3]*256 + (unsigned int)pC[2])*256 
      + (unsigned int)pC[1])*256 + (unsigned int) pC[0];
}


  ///////////////////////////////////////////////////////////////////////////////
  //       Public Functions
  ///////////////////////////////////////////////////////////////////////////////


  //-----------------------------------------------------------------------------
  //    main()
  //
  //    This function reads in an SCP-ECG file from inFile and parses out all of
  //    the header information, ignoring the actual ECG data.
  //-----------------------------------------------------------------------------
  int main(int argc, char *argv[])
  {
	  
	auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
	ostringstream base_dir;
	
	base_dir << "./" << std::put_time(&tm, "%Y%m%d%H%M%S") << "/";

    unsigned int i;
    unsigned int j;

    if(argc < 2)
    {
      cerr << "No input file specified! Aborting." << endl;
      return -1;
    }

    // Open the file and check the file length
    ifstream inFile (argv[1], ios::in | ios::binary);
    inFile.seekg(0, ios::end);
    unsigned int fileLength = inFile.tellg();
    inFile.seekg(0, ios::beg);

    mkdir(base_dir.str().c_str(), DEFFILEMODE);
	

    if (fileLength > 0 && fileLength < SCP_SIZE)
    {
        cerr << "Input file too small! Aborting." << endl;
        return -1;
    }
	
	if ((fileLength % SCP_SIZE) != 0)
	{
		cerr << "Invalid input length: " << fileLength << ", Aborting." << endl;
		return -1;
	}

    unsigned int scp_count = fileLength / SCP_SIZE;
	
	char scp_buf[SCP_SIZE];

	ostringstream fileNameBuilder;
	
    for (i = 0; i < scp_count; i++)
    {
		inFile.seekg(i * SCP_SIZE);
		
        unsigned int dir_names[4];

        dir_names[0] = (i - (i % 1000000)) / 1000000;
        dir_names[1] = ((i % 1000000) - (i % 10000)) / 10000;
        dir_names[2] = ((i % 10000) - (i % 100)) / 100;
        dir_names[3] = (i % 100);
		
		cout << "Sequence: " << setw(8) << setfill('0') << i << ": " ;
		cout << setw(2) << setfill('0') << dir_names[0] << ", ";
		cout << setw(2) << setfill('0') << dir_names[1] << ", ";
		cout << setw(2) << setfill('0') << dir_names[2] << ", ";
		cout << setw(2) << setfill('0') << dir_names[3] << endl;

		fileNameBuilder.str("");
		fileNameBuilder.clear();
		
        fileNameBuilder << base_dir.str();

        for (j = 0; j < 3; j++)
        {
            fileNameBuilder << setw(2) << setfill('0') << dir_names[j] << "/";
            mkdir(fileNameBuilder.str().c_str(), DEFFILEMODE);
        }

        fileNameBuilder << setw(2) << setfill('0') << dir_names[3] << ".scp";

        ofstream outFile(fileNameBuilder.str(), ios_base::out | ios_base::binary);

		inFile.read(&scp_buf[0], SCP_SIZE);
		outFile.write(&scp_buf[0], SCP_SIZE);
        outFile.close();
    }


    cout << "Done.\n" << endl;                 // Signal completion of program

    return 0;
  }



/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       hpr_actions_parser.cpp
 *          - This program parses a *.tza actions file from <inFile> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./hpr_actions_parser inFile.tza > outFile.txt
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>


using namespace std;



/******************************************************************************
 *       Feature Controlling Definitions
 ******************************************************************************/

#define CHECK_CRC                   // Comment this line to disable CRC Checking

#define ACTIONS_ID_STRING          "TZACT"

#define MAX_FILE_SIZE         (4*1024)

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   Possible TAG values for each entry   ***/
enum actionTags {
  ACTIONS_ENABLE_NEW_STUDY         = 10,
  ACTIONS_REQUEST_SCP_BLOCK        = 20,
  ACTIONS_RETRANSMIT_SCP_FILE      = 21,
  ACTIONS_RECENT_SCP               = 22,
  ACTIONS_REQUEST_TZR_FILE         = 30,
  ACTIONS_UPDATE_SETTINGS          = 40,
  ACTIONS_DISPLAY_MESSAGE          = 50,
  ACTIONS_ERROR_LOG                = 60,
  ACTIONS_FIRMWARE                 = 100,
  ACTIONS_END_STUDY                = 200,
  ACTIONS_START_STUDY              = 201,
  ACTIONS_FORMAT_SD                = 220,

  BAD_TAG                          = 254,
  ACTIONS_TERMINATOR               = 255
};


///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  unsigned short ourcrcValue;                  // The CRC we calculate
  unsigned short theircrcValue;                // The CRC from the file
  unsigned int length;                         // The length read from the file

  unsigned char * pRead;                       // Pointer used for the read command
  unsigned short *shortCaster;
  unsigned int *intCaster;

  unsigned char firstBlock[16];

  int i;

  ourcrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff

  string programName(argv[0]);
  string inputName;
  bool displayHelp = 1;

  // Process the command line arguments
  for(i = 0; i < argc; i++){
    if(*argv[i] == '-') switch(*(argv[i]+1)){
      case 'i':
        if(++i >= argc){
          cerr << programName << ": filename must follow -i." << endl;
          return -1;
        }
        inputName.assign(argv[i]);
        displayHelp = 0;
        break;
      default:
        cerr << programName << ": Unexpected option: " << *(argv[i]+1) << endl;
        cerr << "Aborting (3)." << endl;
        return -1;
    }
  }

  // If there were no command line arguments, just display the usage info
  if(displayHelp){
    cout << "Usage: " << programName << " [options] ..." << endl;
    cout << "Options:" << endl;
    cout << "  -i FILE\t\tRead FILE as input to parse." << endl;

    return 1;
  }

  // Always require the user to specify the input file
  if(!inputName.length()){
    cout << "No input file specified! Aborting." << endl;
    return -1;
  }

  fstream inFile (inputName.c_str(), ios::in | ios::binary);
  inFile.seekg(0, ios::end);
  unsigned int fileLength = inFile.tellg();
  inFile.seekg(0, ios::beg);

  if(fileLength > MAX_FILE_SIZE){
    cout << "File Size (" << fileLength << ") is SIGNIFICANTLY larger than expected. Aborting." << endl;
    return -1;
  }

  for(i = 0; i < 16; i++){
    firstBlock[i] = 0;
  }
  inFile.read((char *) firstBlock, 16);           // Read the first block into RAM

  string str;
  str.assign((const char *) &firstBlock[6]);

  if(str.compare(0, 6, ACTIONS_ID_STRING)){
    cerr << "File corrupted! (" << str << ") Aborting." << endl;
    return -1;
  }
  else{
    i = 0;
    shortCaster = (unsigned short *) &firstBlock[i];
    theircrcValue = *shortCaster;
    i += 2;

    intCaster = (unsigned int *) &firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
    for(i = 0; i < 16; i++){
      pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
    }
    inFile.read((char *) &pRead[16], fileLength-16);    // Store the remainder of the file in memory

    crcBlock(&pRead[2], length-2, &ourcrcValue); // Calculate the CRC for the remainder of the file
  }

  cout << "---File Header:" << endl;
#ifdef CHECK_CRC
  if(ourcrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
    cout << "File CRC Valid: 0x" << hex << theircrcValue << dec << endl;  // Notify user that we are parsing
#else
    cout << "Ignoring CRC (0x" << hex << theircrcValue 
      << " : 0x" << ourcrcValue << ")" << dec << endl;    // Ignore the CRC values (debugging ONLY)
#endif

    unsigned int j = 6;

    cout << "File Length: " << length << " Bytes" << endl;// Print out the file length   
    cout << "Format Identifier String: " << &pRead[j] << endl;
    j += 6;
    string device_id((const char *) &pRead[j]);    
    cout << "Device Identifier String: " << device_id << endl;
    j += 6;

    cout << "Firmware Version: " << (pRead[j]/10)         // Print out the Firmware Verison
      << "." << (pRead[j]%10) << endl;
    j += 1;

    if(pRead[j] == '\0'){                                 // Check for a NULL string
      cout << "NULL Serial Number" << endl;              // Report the NULL string
    }
    else{
      cout << "Serial Number: " << &pRead[j] << endl;    // Parse out the Serial Number String
    }
    if(string::npos != device_id.find("HPR"))
    {
      i += 11;
    }
    else
    {
      cerr << "Unrecognized Device ID: " << device_id << endl;
      return -1;
    }

    unsigned short fileID = pRead[j] + ((short) pRead[j+1] << 8);
    j += 2;
    cout << "File Identifier: " << fileID << endl;

    cout << endl;
    cout << "---Settings Entries:" << endl;

    while(j < (length - 2) ){                 // Parse the events until the end of the file
      unsigned int actionTag = pRead[j++];                                // Action Tag
      unsigned int dataLength = pRead[j++];                             // Data Length
      unsigned int temp = 0;
      cout << "(" << setw(3) << actionTag << " [" 
        << setw(3) << dataLength << "])";
      switch(actionTag){
        case ACTIONS_ENABLE_NEW_STUDY:
          cout << "New Study" << endl;
          break;

        case ACTIONS_REQUEST_SCP_BLOCK:
          cout << "SCP block request for files #";
          temp = pRead[j] + (pRead[j+1]<<8) + (pRead[j+2]<<16) + (pRead[j+3]<<24);
          cout << temp << " through #" << (temp + pRead[j+4] - 1) << endl;
          break;

        case ACTIONS_RETRANSMIT_SCP_FILE:
          temp = pRead[j] + (pRead[j+1]<<8) + (pRead[j+2]<<16) + (pRead[j+3]<<24);
          cout << "Retransmit SCP file #" << temp << endl;
          break;
        
        case ACTIONS_RECENT_SCP:
          temp = pRead[j] + (pRead[j+1]<<8);
          cout << "Requested " << temp << " seconds of recent ECG." << endl;
          break;

        case ACTIONS_REQUEST_TZR_FILE:
          cout << "TZR file request for ";
          temp = pRead[j] + (pRead[j+1]<<8);
          cout << temp << "/" << (int) pRead[j+2] << "/" << (int) pRead[j+3];
          cout << " at hour " << (int) pRead[j+4] << endl;
          break;

        case ACTIONS_UPDATE_SETTINGS:
          cout << "Update Settings" << endl;
          break;

        case ACTIONS_DISPLAY_MESSAGE:
          cout << "Display Message: " << (char *) &pRead[j] << endl;
          break;

        case ACTIONS_ERROR_LOG:
          cout << "Error log request." << endl;
          break;

        case ACTIONS_FIRMWARE:
          cout << "Update firmware: " << (char *) &pRead[j] << endl;
          break;

        case ACTIONS_END_STUDY:
          cout << "End Study: " << (int) pRead[j] << endl;
          break;

        case ACTIONS_START_STUDY:
          cout << "Start Study: 0x" << hex << (int) pRead[j] << dec << endl;
          break;

        case ACTIONS_FORMAT_SD:
          cout << "Format SD: 0x" << hex << (int) pRead[j] << dec << endl;
          break;

        case ACTIONS_TERMINATOR:
          cout << "Terminator: " << (int) pRead[j] << endl;
          break;

        default:
          cout << "* ERROR, UNKNOWN TAG!" << endl;
          break;
      }

      j += dataLength;

      if(pRead[j++] != '\0'){
        cout << "Missing NULL detected!" << endl;
      }
    }
#ifdef CHECK_CRC
  }
  else{
    cout << "File CRC Invalid. Halting\n";
    cout << hex << "thier CRC: " << theircrcValue << " - our CRC: " << ourcrcValue << dec << endl;
    cout << "File Length: " << length << endl;            // Print out the file length      
  }
#endif

  delete[] pRead;                  // Free the buffer we used for parsing

  cout << "Done.\n" << endl;                 // Signal completion of program

  return 0;
}



# HPR ACTIONS SPECIFICATION #

## Overview ##

The HPR device will execute actions according to the contents of actions files (\*.tza). On reset the device will search its root directory for any files with the \*.tza suffix and will execute the first such file that it finds. The format of these files is described below.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZACT" to identify the file as an actions file |
| Device String | 6 | The ASCII string "HPR" to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| File Identifier | 2 | File ID # used to distinguish file versions |
| Action 1 | *Variable* | First action entry. Format described below |
| ... | ... | ... |
| Action N | *Variable* | Last action entry. Format described below |
| Terminator | 4 | Terminator entry. Format described below |

## Action Entry Format ##

Each entry in the actions file will contain an instruction for one action to be executed by the HPR. These entries will consist of an identifier byte, a data length byte, a variable number of data bytes, and a reserved byte for internal use by the HPR. The values of the identifier tags may be found below. Each entry will adhere to the following format:

| Item | Bytes | Description |
| --- | --- | --- |
| Actions Tag | 1 | This value identifies the action associated with the data to follow. Possible values are described below. |
| Length | 1 | This is the number of bytes of data associated with this action |
| Data | <_length_> | This is the data required for this action |
| Reserved | 1 | This byte is reserved for use by the HPR and should always be set to 0 |

## Action Tag Tool Tips ##

Possible Actions

### Format microSD Card ###

Action Tag: **220**.

Required Data: **Byte 1: 0x42** - Validity Check.

This action causes the device to self-format the internal microSD card, wiping any data from the card in the process.

### Terminator ###

Action Tag: **255**.

Required Data: **Byte 1: 0xFF** - Unused Validity Check.

This action indicates the end of a string of actions in a TZA file.

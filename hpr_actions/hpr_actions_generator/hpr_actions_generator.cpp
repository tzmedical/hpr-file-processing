/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       hpr_actions_generator.cpp
 *          - This program generates an actions file for the HPR based on the
 *                 values in the input file and outputs the result to <cout>.
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./hpr_actions_generator input.txt > output.tza
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

#include "string.h"

#include <io.h>
#include <fcntl.h>

using namespace std;


/******************************************************************************
*       Feature Controlling Definitions
******************************************************************************/

#define MAX_FILE_LENGTH               32768

#define ACTIONS_ID_STRING           "TZACT"

#define MODEL_DESC_STRING           "HPR  "

#define MAX_SCP_REQUESTS                255
#define MAX_TZR_REQUESTS                255
#define MAX_RETRANSMIT_REQUESTS         9


///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   Possible TAG values for each entry   ***/
enum actionTags {
   ACTIONS_REQUEST_SCP_BLOCK        = 20,
   ACTIONS_RETRANSMIT_SCP_FILE      = 21,
   ACTIONS_REQUEST_RECENT_SCP_BLOCK = 22,
   ACTIONS_REQUEST_TZR_FILE         = 30,
   ACTIONS_UPDATE_SETTINGS          = 40,
   ACTIONS_DISPLAY_MESSAGE          = 50,
   ACTIONS_TAG_REQUEST_ERROR_LOG    = 60,
   ACTIONS_TAG_UPDATE_FIRMWARE      = 100,
   ACTIONS_END_STUDY                = 200,
   ACTIONS_START_STUDY              = 201,
   ACTIONS_TAG_FORMAT_SD            = 220,

   BAD_TAG                          = 254,
   ACTIONS_TERMINATOR               = 255
};


///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
//       Gloabal Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};



///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
   unsigned int j;
   for(j = 0; j < length; j++){
      *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
   }
   return 0;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif

  fstream argumentFile;
  istream *input;

  if(argc >= 2){
    argumentFile.open(argv[1], ios::in);
    input = &argumentFile;
  }
  else{
    input = &cin;
  }

  uint16_t firmwareVersion = 0;
  string serialNumber("");
  uint16_t fileID = 0;

  bool scp_request_enable[MAX_SCP_REQUESTS];
  uint32_t scp_request_sequence_number[MAX_SCP_REQUESTS];
  uint32_t scp_request_file_count[MAX_SCP_REQUESTS];
  bool retransmit_scp_enable[MAX_RETRANSMIT_REQUESTS];
  uint32_t retransmit_scp_sequence_number[MAX_RETRANSMIT_REQUESTS];
  uint16_t recent_scp = 0;
  bool tzr_request_enable[MAX_TZR_REQUESTS];
  uint32_t tzr_request_year[MAX_TZR_REQUESTS];
  uint32_t tzr_request_month[MAX_TZR_REQUESTS];
  uint32_t tzr_request_day[MAX_TZR_REQUESTS];
  uint32_t tzr_request_hour[MAX_TZR_REQUESTS];
  bool update_settings_enable = 0;
  string message_string("");
  string firmware_string("");
  bool error_log_enable = 0;
  bool end_study_enable = 0;
  bool start_study_enable = 0;
  bool format_sd_enable = 0;

  string tempStr;

  uint32_t i, j, k;

  for(j = 0; j < MAX_SCP_REQUESTS; j++){
    scp_request_enable[j] = 0;
    scp_request_sequence_number[j] = 0;
    scp_request_file_count[j] = 0;
  }

  for(j = 0; j < MAX_RETRANSMIT_REQUESTS; j++){
    retransmit_scp_enable[j] = 0;
    retransmit_scp_sequence_number[j] = 0;
  }

  for(j = 0; j < MAX_TZR_REQUESTS; j++){
    tzr_request_enable[j] = 0;
    tzr_request_year[j] = 0;
    tzr_request_month[j] = 0;
    tzr_request_day[j] = 0;
    tzr_request_hour[j] = 0;
  }

  // Loop through the entire input
  while(!input->eof()){
    string argument;
    char cstr[1024];

    // Get a single line from the input (max length: 1024 characters)
    input->getline(cstr, 1024);
    argument.assign(cstr);

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if(begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if(endStr != string::npos) argument.resize(endStr+1);

    // Skip the line if it is a comment
    if(argument.find("#") == 0){
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if(argument.length() > 8){
      // Trim whitespace before or after the "="
      bool done = 0;
      while(!done){
        size_t found;
        if((found = argument.find(" =")) != string::npos){
          argument.erase(found, 1);
        }
        else if((found = argument.find("\t=")) != string::npos){
          argument.erase(found, 1);
        }
        else if((found = argument.find("= ")) != string::npos){
          argument.erase(found+1, 1);
        }
        else if((found = argument.find("=\t")) != string::npos){
          argument.erase(found+1, 1);
        }
        else{
          done = 1;
        }
      }

      // Locate and parse any file-related parameters
      if(argument.find("file.") != string::npos){
        // Sets the firmware version embedded in the header of the file
        if(argument.find("firmware_version=") != string::npos){
          tempStr.assign(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            stringstream argStream(tempStr);
            argStream >> firmwareVersion;
          }
        }
        // Sets the serial number embedded in the header of the file
        else if(argument.find("serial_number=") != string::npos){
          serialNumber.assign(argument.substr(argument.find("=")+1));
          if(serialNumber.length() && (serialNumber.length() != 7)){
            cerr << "ERROR! Invalid Serial Number: " << argument << endl;
            return -1;
          }
        }
        // Sets the file ID embedded in the header of the file
        else if(argument.find("file_id=") != string::npos){
          tempStr.assign(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            stringstream argStream(tempStr);
            argStream >> fileID;
          }
        }
        // Abort if an invalid file setting is detected
        else{
          cerr << "ERROR! Unrecognized input: " << argument << endl;
          return -1;
        }
      }
      // Locate and process any actions from the file
      else if(argument.find("action.") != string::npos){
        if(argument.find("scp_request.") != string::npos){
          uint32_t idx = 0;
          tempStr.assign(argument.substr(argument.find("scp_request.")+12));
          if(tempStr.length()){
            stringstream tempstream(tempStr);
            tempstream >> idx;
            if(idx >= MAX_SCP_REQUESTS){
              cerr << "ERROR! Invalid SCP request idx (" << idx << "): " << argument << endl;
              return -1;
            }
            if(argument.find("enable=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                if(tempStr.find("true") != string::npos){
                  scp_request_enable[idx] = 1;
                }
                else if(tempStr.find("false") != string::npos){
                  scp_request_enable[idx] = 0;
                }
                else{
                  cerr << "ERROR! Invalid string: " << argument << endl;
                  return -1;
                }
              }
            }
            else if(argument.find("sequence_number=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> scp_request_sequence_number[idx];
              }
            }
            else if(argument.find("file_count=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> scp_request_file_count[idx];
                if(!scp_request_file_count[idx]){
                  cerr << "ERROR! SCP request for 0 files: " << argument << endl;
                  return -1;
                }
              }
            }
            else{
              cerr << "ERROR! Unrecognized input: " << argument << endl;
              return -1;
            }
          }
        }
        else if(argument.find("retransmit_scp.") != string::npos){
          uint32_t idx = 0;
          tempStr.assign(argument.substr(argument.find("retransmit_scp.")+15));
          if(tempStr.length()){
            stringstream tempstream(tempStr);
            tempstream >> idx;
            if(idx >= MAX_RETRANSMIT_REQUESTS){
              cerr << "ERROR! Invalid SCP retransmit idx (" << idx << "): " << argument << endl;
              return -1;
            }
            if(argument.find("enable=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                if(tempStr.find("true") != string::npos){
                  retransmit_scp_enable[idx] = 1;
                }
                else if(tempStr.find("false") != string::npos){
                  retransmit_scp_enable[idx] = 0;
                }
                else{
                  cerr << "ERROR! Invalid string: " << argument << endl;
                  return -1;
                }
              }
            }
            else if(argument.find("sequence_number=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> retransmit_scp_sequence_number[idx];
              }
            }
            else{
              cerr << "ERROR! Unrecognized input: " << argument << endl;
              return -1;
            }
          }
        }
        else if(argument.find("recent_scp=") != string::npos){
          stringstream tempstream;
          tempStr.assign(argument.substr(argument.find("recent_scp=")+11));
          if(tempStr.length()){
            tempstream << tempStr;
            tempstream >> recent_scp;
          }
        }
        else if(argument.find("tzr_request.") != string::npos){
          uint16_t idx = 12;
          stringstream tempstream;
          tempStr.assign(argument.substr(argument.find("tzr_request.")+12));
          if(tempStr.length()){
            tempstream << tempStr;
            tempstream >> idx;
            if(idx >= MAX_RETRANSMIT_REQUESTS){
              cerr << "ERROR! Invalid TZR request idx (" << idx << "): " << argument << endl;
              return -1;
            }
            if(argument.find("enable=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                if(tempStr.find("true") != string::npos){
                  tzr_request_enable[idx] = 1;
                }
                else if(tempStr.find("false") != string::npos){
                  tzr_request_enable[idx] = 0;
                }
                else{
                  cerr << "ERROR! Invalid string: " << argument << endl;
                  return -1;
                }
              }
            }
            else if(argument.find("year=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> tzr_request_year[idx];
              }
            }
            else if(argument.find("month=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> tzr_request_month[idx];
              }
            }
            else if(argument.find("day=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> tzr_request_day[idx];
              }
            }
            else if(argument.find("hour=") != string::npos){
              tempStr.assign(argument.substr(argument.find("=")+1));
              if(tempStr.length()){
                stringstream argStream(tempStr);
                argStream >> tzr_request_hour[idx];
              }
            }
            else{
              cerr << "ERROR! Unrecognized input: " << argument << endl;
              return -1;
            }
          }
        }
        else if(argument.find("update_settings.") != string::npos){
          if(argument.find("enable=") != string::npos){
            tempStr.assign(argument.substr(argument.find("=")+1));
            if(tempStr.length()){
              if(tempStr.find("true") != string::npos){
                update_settings_enable = 1;
              }
              else if(tempStr.find("false") != string::npos){
                update_settings_enable = 0;
              }
              else{
                cerr << "ERROR! Invalid string: " << argument << endl;
                return -1;
              }
            }
          }
          else{
            cerr << "ERROR! Unrecognized input: " << argument << endl;
            return -1;
          }

        }
        else if(argument.find("send_message=") != string::npos){
          message_string.assign(argument.substr(argument.find("=")+1));
        }
        else if(argument.find("error_log.") != string::npos){
          if(argument.find("enable=") != string::npos){
            tempStr.assign(argument.substr(argument.find("=")+1));
            if(tempStr.length()){
              if(tempStr.find("true") != string::npos){
                error_log_enable = 1;
              }
              else if(tempStr.find("false") != string::npos){
                error_log_enable = 0;
              }
              else{
                cerr << "ERROR! Invalid string: " << argument << endl;
                return -1;
              }
            }
          }
          else{
            cerr << "ERROR! Unrecognized input: " << argument << endl;
            return -1;
          }
        }
        else if(argument.find("firmware_update=") != string::npos){
          firmware_string.assign(argument.substr(argument.find("=")+1));
        }

        else if(argument.find("end_study.") != string::npos){
          if(argument.find("enable=") != string::npos){
            tempStr.assign(argument.substr(argument.find("=")+1));
            if(tempStr.length()){
              if(tempStr.find("true") != string::npos){
                end_study_enable = 1;
              }
              else if(tempStr.find("false") != string::npos){
                end_study_enable = 0;
              }
              else{
                cerr << "ERROR! Invalid string: " << argument << endl;
                return -1;
              }
            }
          }
          else{
            cerr << "ERROR! Unrecognized input: " << argument << endl;
            return -1;
          }

        }
        else if(argument.find("start_study.") != string::npos){
          if(argument.find("enable=") != string::npos){
            tempStr.assign(argument.substr(argument.find("=")+1));
            if(tempStr.length()){
              if(tempStr.find("true") != string::npos){
                start_study_enable = 1;
              }
              else if(tempStr.find("false") != string::npos){
                start_study_enable = 0;
              }
              else{
                cerr << "ERROR! Invalid string: " << argument << endl;
                return -1;
              }
            }
          }
          else{
            cerr << "ERROR! Unrecognized input: " << argument << endl;
            return -1;
          }

        }
        else if(argument.find("format_sd.") != string::npos){
          if(argument.find("enable=") != string::npos){
            tempStr.assign(argument.substr(argument.find("=")+1));
            if(tempStr.length()){
              if(tempStr.find("true") != string::npos){
                format_sd_enable = 1;
              }
              else if(tempStr.find("false") != string::npos){
                format_sd_enable = 0;
              }
              else{
                cerr << "ERROR! Invalid string: " << argument << endl;
                return -1;
              }
            }
          }
          else{
            cerr << "ERROR! Unrecognized input: " << argument << endl;
            return -1;
          }

        }

        else{
          cerr << "ERROR! Unrecognized Input: " << argument << endl;
          return -1;
        }

      }
      else{
        cerr << "ERROR! Unrecognized input: " << argument << endl;
        return -1;
      }
    }
  }
  

  unsigned int length;                         // The length read from the file

  unsigned char buffer[MAX_FILE_LENGTH];    //

  i = 6;                        // The first 6 bytes are length and CRC
  //    Add these at the end

  strncpy((char *) &buffer[i], ACTIONS_ID_STRING, 6);  // The format identifier string
  i += 6;

  strncpy((char *) &buffer[i], MODEL_DESC_STRING, 6);   // The device ID string
  i += 6;

  buffer[i++] = firmwareVersion;             // This byte is F. Vers. (*10)

  strncpy((char *) &buffer[i], serialNumber.c_str(), 11);      // The next 11 bytes are the serial number
  i += 11;                                // Increment the pointer

  unsigned char *caster = (unsigned char *) &fileID;

  buffer[i++] = caster[0];
  buffer[i++] = caster[1];

  // SCP_REQUEST - request a block of 1 to 255 SCP files per request
  for(j = 0; j < MAX_SCP_REQUESTS; j++){
    if(scp_request_enable[j]){
      buffer[i++] = ACTIONS_REQUEST_SCP_BLOCK;
      buffer[i++] = 5;
      uint32_t temp = scp_request_sequence_number[j];
      for(k = 0; k < 4; k++){
        buffer[i++] = temp & 0xff;
        temp = temp >> 8;
      }
      buffer[i++] = scp_request_file_count[j];
      buffer[i++] = '\0';
    }
  }

  // RETRANSMIT_SCP - forces retransmission of a corrupt file, regardless of
  // the upload log (no event associated with this upload...)
  for(j = 0; j < MAX_RETRANSMIT_REQUESTS; j++){
    if(retransmit_scp_enable[j]){
      buffer[i++] = ACTIONS_RETRANSMIT_SCP_FILE;
      buffer[i++] = 4;
      uint32_t temp = retransmit_scp_sequence_number[j];
      for(k = 0; k < 4; k++){
        buffer[i++] = temp & 0xff;
        temp = temp >> 8;
      } 
      buffer[i++] = '\0';
    }
  }

  // RECENT SCP - requests the most recent SCP files from the device
  if(recent_scp)
  {
    buffer[i++] = ACTIONS_REQUEST_RECENT_SCP_BLOCK;
    buffer[i++] = 2;
    buffer[i++] = recent_scp & 0xff;
    buffer[i++] = (recent_scp >> 8) & 0xff;
    buffer[i++] = '\0';
  }

  // TZR_REQUEST - requests a specific TZR file from the device
  for(j = 0; j < MAX_TZR_REQUESTS; j++){
    if(tzr_request_enable[j]){
      if( (tzr_request_year[j] < 2010) || (tzr_request_year[j] > 2100) ){
        cerr << "ERROR! Invalid interval request year (" << j << "): " << tzr_request_year[j] << endl;
        return -1;
      }
      if( !tzr_request_month[j] || (tzr_request_month[j] > 12) ){
        cerr << "ERROR! Invalid interval request month (" << j << "): " << tzr_request_month[j] << endl;
        return -1;
      }
      uint32_t max = 0;
      switch(tzr_request_month[j]){
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
          max = 31;
          break;
        case 4: case 6: case 9: case 11:
          max = 30;
          break;
        case 2:
          if( !(tzr_request_year[j]%4) && (tzr_request_year[j]%100) ){
            max = 29;
          }
          else{
            max = 28;
          }
          break;
      }
      if( !tzr_request_day[j] || (tzr_request_day[j] > max) ){
        cerr << "ERROR! Invalid interval request day (" << j << "): " << tzr_request_day[j] << endl;
        return -1;
      }
      if(tzr_request_hour[j] > 23){
        cerr << "ERROR! Invalid interval request hour (" << j << "): " << tzr_request_hour[j] << endl;
        return -1;
      }
      buffer[i++] = ACTIONS_REQUEST_TZR_FILE;
      buffer[i++] = 5;
      uint32_t temp = tzr_request_year[j];
      for(k = 0; k < 2; k++){
        buffer[i++] = temp & 0xff;
        temp = temp >> 8;
      } 
      buffer[i++] = tzr_request_month[j];
      buffer[i++] = tzr_request_day[j];
      buffer[i++] = tzr_request_hour[j];
      buffer[i++] = '\0';
    }
  }

  // UPDATE_SETTINGS - tell the device to download the settings file from the
  // server immediately
  if(update_settings_enable){
    buffer[i++] = ACTIONS_UPDATE_SETTINGS;
    buffer[i++] = 0;
    buffer[i++] = '\0';
  }

  // SEND_MESSAGE - displays an SMS-like message on the secreen
  if(message_string.length() && (message_string.length() <= 255)){
    buffer[i++] = ACTIONS_DISPLAY_MESSAGE;
    buffer[i++] = message_string.length();
    for(k = 0; k < message_string.length(); k++){
      buffer[i++] = message_string[k];
    }
    buffer[i++] = '\0';
  }

  // ERROR_LOG - upload an error log to the server
  if(error_log_enable){
    buffer[i++] = ACTIONS_TAG_REQUEST_ERROR_LOG;
    buffer[i++] = 0;
    buffer[i++] = '\0';
  }

  // FIRMWARE_UPDATE - 
  if(firmware_string.length() && (firmware_string.length() <= 255)){
    buffer[i++] = ACTIONS_TAG_UPDATE_FIRMWARE;
    buffer[i++] = firmware_string.length();
    for(k = 0; k < firmware_string.length(); k++){
      buffer[i++] = firmware_string[k];
    }
    buffer[i++] = '\0';
  }

  // END_STUDY - the alternative to the early-out procedure
  if(end_study_enable){
    buffer[i++] = ACTIONS_END_STUDY;
    buffer[i++] = 1;
    buffer[i++] = 0xa5;
    buffer[i++] = '\0';
  }

  // START_STUDY - the alternative to entering the PIN code
  if(start_study_enable){
    buffer[i++] = ACTIONS_START_STUDY;
    buffer[i++] = 1;
    buffer[i++] = 0x5a;
    buffer[i++] = '\0';
  }

  // FORMAT_SD - Format the SD card
  if(format_sd_enable){
    buffer[i++] = ACTIONS_TAG_FORMAT_SD;
    buffer[i++] = 1;
    buffer[i++] = 0x42;
    buffer[i++] = '\0';
  }

  // Always end the file with the terminator
  buffer[i++] = ACTIONS_TERMINATOR;
  buffer[i++] = 1;
  buffer[i++] = 0xff;
  buffer[i++] = '\0';


  // Finish the length/CRC in the header
  length = i;

  i = 2;
  uint32_t temp = length;
  for(j = 0; j < 4; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  uint16_t crcValue = 0xffff;

  crcBlock(&buffer[2], length - 2, &crcValue);

  i = 0;
  temp = crcValue;
  for(j = 0; j < 2; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  // Write the binary file to cout (should be redirected to a file...)
  cout.write((const char *) buffer, length);

  return 0;
}




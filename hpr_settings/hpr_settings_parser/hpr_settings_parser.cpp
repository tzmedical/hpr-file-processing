/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       hpr_settings_parser.cpp
 *          - This program parses a *.tzs settings file from <cin> and
 *            outputs a text interpretation of each entry on <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./hpr_settings_parser inFile.tzs > outFile.txt
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

#include <inttypes.h>

#include <io.h>
#include <fcntl.h>

using namespace std;



/******************************************************************************
 *       Feature Controlling Definitions
 ******************************************************************************/

#define ENABLE_RATE_CHANGE          (1)

#define CHECK_CRC                   // Comment this line to disable CRC Checking

#define SETTINGS_ID_STRING    "TZSET"

#define MAX_FILE_SIZE         (4*1024)


///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   Possible TAG values for each entry   ***/
enum settingTags {
  BRADY_BPM                  = 1,
  TACHY_BPM                  = 4,
  PAUSE_DURATION             = 8,
#if ENABLE_RATE_CHANGE == 1
  BRADY_RATE_CHANGE          = 10,
  TACHY_RATE_CHANGE          = 11,
#endif
  ARRHYTHMIA_MIN_DURATION    = 12,
  DETECT_PACEMAKER           = 31,
  SUMMARIZE_QRS_DETECTION    = 40,
  PEDOMETER_PERIOD           = 41,
  STORE_RAW_ACCEL            = 42,
  LENGTH_IS_CALENDAR_DAYS    = 95,
  ONE_ECG_FILE               = 96,
  STUDY_HOURS                = 97,
  SAMPLE_RATE                = 101,
  REPORT_PRE_TIME            = 105,
  REPORT_POST_TIME           = 106,
  DIGITAL_HP_FILTER          = 111,
  START_OK_CODE              = 112,
  DIGITAL_LP_FILTER          = 113,
  DIGITAL_NOTCH_FILTER       = 114,
  NAG_ON_REPLACE_ELECTRODE   = 144,
  NAG_ON_LOW_BATTERY         = 145,
  PATCH_MAX_WEAR_LENGTH      = 146,
  ALLOW_LOW_BATTERY_START    = 147,
  STUDY_COMPLETE_SLEEP       = 148,
  NAG_ON_LEAD_OFF            = 149,
  BULK_UPLOAD                = 155,
  TRANSMIT_INTERVAL_REPORTS  = 156,
  DEMO_MODE                  = 160,
  MODEM_TEMPERATURE          = 171,
  PATIENT_ID                 = 201,
  TOP_BANNER                 = 211,
  SERVER_ADDRESS             = 213,
  TLS_IDENTITY               = 214,
  TLS_PSK                    = 215,
  APN_ADDRESS                = 216,
  APN_USERNAME               = 217,
  APN_PASSWORD               = 218,
  HTTP_BEARER_TOKEN          = 219,
  CONNECTION_TIMEOUT         = 225,
  ERROR_RETRIES              = 251,
  ERROR_PERIOD               = 252,
  ZYMED_COMPAT               = 253,
  BAD_TAG                    = 254,
  TERM_TAG                   = 255
};


///////////////////////////////////////////////////////////////////////////////
//       Global Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////

class _setting
{
  private:
    uint64_t min, max;
    uint64_t dataVal;
    settingTags tag;
    uint8_t maxLength;
    uint8_t dataLength;
    uint8_t binaryFlag;
    uint8_t setFlag;
    uint8_t minFW;
    uint8_t maxFW;
    string dataStr;

  public:
     _setting(const char *pS, settingTags tg, uint64_t mn, uint64_t mx, uint8_t len,
        uint8_t min_fw, uint8_t max_fw)
    {
      parseStr.assign(pS);
      tag = tg;
      min = mn;
      max = mx;
      dataVal = 0;
      maxLength = len;
      dataLength = len;
      dataStr.assign("");
      binaryFlag = 1;
      setFlag = 0;
      minFW = min_fw;
      maxFW = max_fw;
    }

    _setting(const char *pS, settingTags tg, uint8_t len,
        uint8_t min_fw, uint8_t max_fw)
    {
      parseStr.assign(pS);
      tag = tg;
      min = 0;
      max = 0;
      dataVal = 0;
      maxLength = len;
      dataLength = 0;
      dataStr.assign("");
      binaryFlag = 0;
      setFlag = 0;
      minFW = min_fw;
      maxFW = max_fw;
    }

    string parseStr;

    int set(string dS, bool check_range){
      if(binaryFlag){
        stringstream argStream;
        argStream << dS;
        argStream >> dataVal;
        dataStr.assign("");
        if( check_range && ((dataVal < min) || (dataVal > max)) ){
          cerr << "ERROR! Invalid binary data: setting." << parseStr << dataVal << endl;
          return -1;
        }

      }
      else{
        dataStr.assign(dS);
        dataVal = 0;
        dataLength = 0;
        if( check_range && (dataStr.length() > maxLength) ){
          cerr << "ERROR! Invalid string data: setting." << parseStr << dataStr << endl;
          cerr << "---Max Length: " << (int) maxLength << endl;
          cerr << "---Input Length: " << (int) dataStr.length() << endl;
          return -1;
        }
      }

      setFlag = 1;
      return 0;
    }

    int write(uint8_t *pBuffer, bool badAlignment, uint8_t firmwareVersion){
      int i = 0;
      uint32_t j;

      if(setFlag && (firmwareVersion >= minFW) && (firmwareVersion <= maxFW) ){
        pBuffer[i++] = tag;

        if(!binaryFlag){
          pBuffer[i++] = dataStr.length();

          for(j = 0; j < dataStr.length(); j++){
            pBuffer[i++] = dataStr[j];
          }
        }
        else{
          pBuffer[i++] = dataLength;

          uint64_t temp = dataVal;
          for(j = 0; j < dataLength; j++){
            pBuffer[i++] = temp & 0xff;
            temp = temp >> 8;
          }
        }

        if(badAlignment) pBuffer[i++] = 0xc4;
        else pBuffer[i++] = '\0';
      }

      return i;
    }

    unsigned int get_tag(){
      return (int) tag;
    }

    string getParseStr(){
      return parseStr;
    }

    bool isBinary(){
      return binaryFlag;
    }

    uint32_t getLength(){
      return maxLength;
    }

    int checkFirmwareVersion(uint8_t firmware_version)
    {
      if(firmware_version < minFW)
      {
        cerr << "ERROR! Setting #" << (int) tag 
             << " is not supported in versions below " << minFW << endl;
        return -1;
      }
      else if(firmware_version > maxFW)
      {
        cerr << "ERROR! Setting #" << (int) tag 
             << " is not supported in versions above " << maxFW << endl;
        return -1;
      }
      else
      {
        return 0;
      }
    }
};

_setting settingList[] = {
  _setting(".detect_pacemaker=", DETECT_PACEMAKER, 0, 1, 1, 10, 255),
  _setting(".pedometer_period=", PEDOMETER_PERIOD, 0, 900, 2, 10, 255),
  _setting(".store_raw_accel=", STORE_RAW_ACCEL, 0, 1, 1, 10, 255),
  _setting(".length_is_calendar_days=", LENGTH_IS_CALENDAR_DAYS, 0, 1, 1, 13, 255),
  _setting(".one_ecg_file=", ONE_ECG_FILE, 0, 1, 1, 10, 255),
  _setting(".study_hours=", STUDY_HOURS, 1, 24*31, 2, 10, 255),
  _setting(".sample_rate=", SAMPLE_RATE, 200, 1600, 2, 10, 255),
  _setting(".digital_hp_filter=", DIGITAL_HP_FILTER, 0, 100, 1, 10, 255),
  _setting(".digital_lp_filter=", DIGITAL_LP_FILTER, 0, 100, 1, 10, 255),
  _setting(".digital_notch_filter=", DIGITAL_NOTCH_FILTER, 0, 100, 1, 10, 255),
  _setting(".nag_on_replace_electrode=", NAG_ON_REPLACE_ELECTRODE, 0, 900,  2, 13, 255),
  _setting(".nag_on_low_battery=", NAG_ON_LOW_BATTERY, 0, 900,  2, 13, 255),
  _setting(".electrode_max_wear_hours=", PATCH_MAX_WEAR_LENGTH, 0, 168, 1, 13, 255),
  _setting(".allow_low_battery_start=", ALLOW_LOW_BATTERY_START, 0, 1, 1, 10, 255),
  _setting(".study_complete_sleep=", STUDY_COMPLETE_SLEEP, 0, 900, 2, 13, 255),
  _setting(".nag_on_lead_off=", NAG_ON_LEAD_OFF, 0, 900,  2, 13, 255),
  _setting(".demo_mode=", DEMO_MODE, 0, 2, 1, 10, 255),
  _setting(".patient_id=", PATIENT_ID, 40, 10, 255),
  _setting(".error_retries=", ERROR_RETRIES, 0, 250, 1, 10, 255),
  _setting(".error_period=", ERROR_PERIOD, 1, 1440, 2, 10, 255),
  _setting(".zymed_compat=", ZYMED_COMPAT, 0, 1, 1, 13, 255),
  _setting(".bad_tag=", BAD_TAG, 0, 1, 1, 10, 255)
};

///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif

  unsigned short ourcrcValue;                  // The CRC we calculate
  unsigned short theircrcValue;                // The CRC from the file
  unsigned int length;                         // The length read from the file

  unsigned char * pRead;                       // Pointer used for the read command
  unsigned short *shortCaster;
  unsigned int *intCaster;

  unsigned char firstBlock[16];

  unsigned int i;


  ourcrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff

  if(argc < 2){
    cerr << "No input file specified! Aborting." << endl;
    return -1;
  }

  fstream inFile (argv[1], ios::in | ios::binary);
  inFile.seekg(0, ios::end);
  unsigned int fileLength = inFile.tellg();
  inFile.seekg(0, ios::beg);

  if(fileLength > MAX_FILE_SIZE){
    cerr << "File Size is SIGNIFICANTLY larger than expected. Aborting." << endl;
    return -1;
  }

  for(i = 0; i < 16; i++){
    firstBlock[i] = 0;
  }
  inFile.read((char *) firstBlock, 16);           // Read the first block into RAM

  cout << "# Lines starting with \"#\" are comments" << endl;
  cout << "# This is an output file from settings_parser.exe" << endl;
  cout << "# This file may be used an an input file for settings_generator.exe" << endl;
  cout << endl;

  string str;
  str.assign((const char *) &firstBlock[6]);
  if(str.compare(0, 6, SETTINGS_ID_STRING)){
    cerr << "ERROR: File corrupted! Aborting." << endl;
    return -1;
  }
  else{
    i = 0;
    shortCaster = (unsigned short *) &firstBlock[i];
    theircrcValue = *shortCaster;
    i += 2;

    intCaster = (unsigned int *) &firstBlock[i];
    length = *intCaster;
    i += 4;

    pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
    for(i = 0; i < 16; i++){
      pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
    }
    inFile.read((char *) &pRead[16], fileLength-16);    // Store the remainder of the file in memory

    crcBlock(&pRead[2], length-2, &ourcrcValue); // Calculate the CRC for the remainder of the file
  }

#ifdef CHECK_CRC
  if(ourcrcValue == theircrcValue){            // If the CRC doesn't match, something has gone wrong
    cout << "#Valid CRC: 0x" << hex << theircrcValue << dec << endl;  // Notify user that we are parsing
#else
    cout << "#Ignoring CRC (0x" << hex << theircrcValue 
      << " : 0x" << ourcrcValue << ")" << dec << endl;    // Ignore the CRC values (debugging ONLY)
#endif

    unsigned int j = 6;

    cout << "#File Length: " << length << " Bytes" << endl;// Print out the file length   
    cout << "#Format Identifier String: " << &pRead[j] << endl;
    j += 6;
    cout << "#Device ID: " << &pRead[j] << endl;
    string device_id((const char *) &pRead[j]);
    j += 6;

    uint8_t firmwareVersion = pRead[j];
    cout << "file.firmware_version=" << dec << (int) firmwareVersion << endl;
    j += 1;

    cout << "file.serial_number=" << &pRead[j] << endl;
    j += 11;

    unsigned short fileID = pRead[j] + ((short) pRead[j+1] << 8);
    j += 2;
    cout << "file.file_id=" << fileID << endl;
    cout << "file.check_setting_ranges=true" << endl;

    cout << endl;

    while(j < (length - 2) ){                 // Parse the events until the end of the file
      unsigned int settingTag = pRead[j++];                                // Setting Tag
      unsigned int settingLength = pRead[j++];                             // Setting Length

      bool matchFound = 0;
      for(i = 0; i < (sizeof(settingList)/sizeof(_setting)); i++){
        if(settingTag == settingList[i].get_tag()){
          matchFound = 1;
          break;
        }
      }

      if(matchFound){
        // Check the version of the setting, but don't abort if there's an
        // error. 
        settingList[i].checkFirmwareVersion(firmwareVersion);

        cout << "setting" << settingList[i].getParseStr();

        uint32_t settingLength = settingList[i].getLength();

        if(settingList[i].isBinary()){
          uint64_t temp = 0;

          for(i = 0; i < settingLength; i++){
            if(i < 8){
              temp += ((uint64_t)pRead[j+i]) << (8*i);
            }
          }

          cout << temp << endl;
        }
        else{
          string tempStr((const char *) &pRead[j]);

          cout << tempStr << endl;
        }
      }
      else if(settingTag != TERM_TAG){
        cerr << "ERROR! Unidentified tag: " << settingTag << endl;
      }

      j += settingLength;

      if(pRead[j++] != '\0'){
        cerr << "Missing NULL detected!" << endl;
      }
    }
#ifdef CHECK_CRC
  }
  else{
    cerr << "File CRC Invalid. Halting\n";
    cerr << hex << "thier CRC: " << theircrcValue << " - our CRC: " << ourcrcValue << dec << endl;
    cerr << "File Length: " << length << endl;            // Print out the file length      
  }
#endif

  delete[] pRead;                  // Free the buffer we used for parsing

  return 0;
}



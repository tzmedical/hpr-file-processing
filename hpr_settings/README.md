# HPR SETTINGS SPECIFICATION #

## Overview ##

Settings for the HPR can be controlled using a settings file copied onto the SD card. The filename for the settings file will be the “config.tzs”. If this file is not present, the device will search for other files named “\*.tzs” and copy the contents of the first valid one it finds to “config.tzs”.

Whenever a settings file is parsed by the device, all settings are first reset to the values stored in internal memory. These will be the listed default values until they are changed. After parsing a settings file, the new values will be saved to internal memory as the new reset values.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZSET" to identify the file as an setting file |
| Device String | 6 | The ASCII string "HPR" to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| File Identifier | 2 | File ID # used to distinguish file versions |
| Setting 1 | *Variable* | First setting entry. Format described below |
| ... | ... | ... |
| Setting N | *Variable* | Last setting entry. Format described below |
| Terminator | 4 | Terminator entry. Format described below |

## Setting Entry Format ##

The settings file will adhere to the following format. Each settings file may vary in length depending on the number of settings to be changed. Any setting not explicitly specified will be set to the default value. Therefore, it is advisable that every setting that could affect the performance of the device should be included in the settings file.

| Item | Bytes | Description |
| --- | --- | --- |
| Settings Tag | 1 | This value identifies the setting associated with the data to follow. Possible values are described below. |
| Length | 1 | This is the number of bytes of data associated with this setting |
| Data | <_length_> | This is the data to be entered for the setting. Numeric values are stored in Little-Endian format |
| Reserved | 1 | This is a '\0' used to identify the end of each entry |

## Settings Terminator Entry ##

The last entry in the settings file will be followed by a 4 byte terminator entry with the following format:

| Item | Bytes | Description |
| --- | --- | --- |
| Settings Tag | 1 | 255 |
| Length | 1 | 1 |
| Data | 1 | 255 |
| Reserved | 1 | 0 |

## Settings Tags ##

Below is a comprehensive list of available settings. The settings are grouped according to types.

## Recording Settings ##

### Detect Pacemaker Spikes ###

Setting Number: **31**.

Value Range: **Enable, Disable**.
Units: **Boolean**

Default: **Disable**.

When this value is set, the device will detect and record the width of pacemaker pulses.

### Pedometer Period ###

Setting Number: **41**.

Value Range: **0 - 900.**.
Units: **Seconds**

Default: **30**.

When this is set to a non-zero value, the device will record the number of detected steps every N seconds in the interval files.

### Study Length is Calendar Days ###

Setting Number: **95**.

Value Range: **Enable, Disable**.
Units: **Boolean**

Default: **Disable**.

When this value is set, the device will include recording interruptions in the
recording length calculation, resulting in the recording ending exactly Study
Hours after it is started, regardless of the amount of data stored.

### One ECG File ###

Setting Number: **96**.

Value Range: **Enable, Disable**.
Units: **Boolean**

Default: **Disable**.

When this value is set, the device will store all ECG data in a single binary file consisting of concatenated SCP files, instead of using the directory structure for individual SCP files.

### Study Hours ###

Setting Number: **97**.

Value Range: **1 - 744**.
Units: **Hours**

Default: **24**.

This setting defines the number of hours of data that the device will record before stopping the recording.

### Sample Rate ###

Setting Number: **101**.

Value Range: **200 - 1600**.
Units: **Hz**

Default: **200**.

The value in this field **_DOES NOTHING_** besides changing how long to wait for more data. The device will always sample at 200 Hz. **_NOTE_**: It is highly recommended to not change this setting. Ever.

### High Pass Digital Filter ###

Setting Number: **111**.

Value Range: **0 - 100**.
Units: **Hz / 100**

Default: **5**.

When this is set to a non-zero value, the device will apply a 2nd order high pass digital filter to the ECG data before being stored to the SD Card. The cutoff frequency in Hz will be the value of this setting divided by 100, with an effective setting range of 0.01 Hz to 1.00 Hz.

### Digital Low Pass Filter ###

Setting Number: **113**.

Value Range: **0 - 100**.
Units: **Hz**

Default: **40**.

When this is set to a non-zero value, the device will apply a 6th order low pass digital filter to the ECG data before being passed to the analysis algorithm or being stored to the SD Card.

### Digital Notch Filter ###

Setting Number: **114**.

Value Range: **0 - 100**.
Units: **Hz**

Default: **60**.

When this is set to a non-zero value, the device will apply a 2nd order notch digital filter to the ECG data before being passed to the analysis algorithm or being stored to the SD Card.

### Zymed Compatability ###

Setting Number: **253**.

Value Range: **Enable, Disable**.
Units: **Boolean**

Default: **Disable**.

When this value is set, the device will store ECG data both in SCP files and
in a <serialNumber>.rf file for import into Zymed software.


## Operating Mode Settings ##

### Allow Low Battery Monitoring Start ###

Setting Number: **147**.

Value Range: **Enable, Disable**.
Units: **Boolean**

Default: **Disable**.

When this is enabled, the device will allow the user to start a study even if the battery is insufficiently charged for the duration of the recording.

### Demonstration Mode ###

Setting Number: **160**.

Value Range: **0 - 10**.
Units: **Seconds**

Default: **0**.

When set to 0, the device operates normally. When set to non-zero, the device will blink the Green LED with a period of 2.4 seconds + this setting value and it is impossible to start a recording.

### Error Retry Count ###

Setting Number: **251**.

Value Range: **0 - 250**.
Units: **Retries**

Default: **5**.

This value defines how many times the device will attempt to automatically recover from an error, within a given time-period, before reporting the error to the user.

### Error Retry Period ###

Setting Number: **252**.

Value Range: **0 - 1440**.
Units: **Minutes**

Default: **30**.

This value defines the period within which the Error Retry Count must not be exceeded.


## Personalization Settings ##

### Patient ID ###

Setting Number: **201**.

Value Range: **40**.
Units: **Characters**

Default: **(BLANK)**.

Up to 40 ASCII characters identifying the patient to the host software system. This ID will be embedded in each SCP-ECG data file.


## Misc Settings ##

### Terminator ###

Setting Number: **255**.

Value Range: **255**.
Units: **<N/A>**

Default: **255**.

This setting indicates the end of a TZS file.

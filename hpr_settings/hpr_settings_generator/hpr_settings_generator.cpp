/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       hpr_settings_generator.cpp
 *          - This program generates a settings file for the HPR based on the
 *                values in the input file and outputs the result to <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make:
 *                make
 *          - call syntax is:
 *                ./hpr_settings_generator input.txt > output.tzs
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

#include <string.h>

#include <inttypes.h>

#include <io.h>
#include <fcntl.h>

using namespace std;



/******************************************************************************
 *       Feature Controlling Definitions
 ******************************************************************************/

#define ENABLE_RATE_CHANGE          (1)

#define MAX_FILE_LENGTH                4096

#define SETTINGS_ID_STRING          "TZSET"

#define MODEL_DESC_STRING           "HPR  "


///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////

/***   Possible TAG values for each entry   ***/
enum settingTags {
  DETECT_PACEMAKER           = 31,
  PEDOMETER_PERIOD           = 41,
  STORE_RAW_ACCEL            = 42,
  LENGTH_IS_CALENDAR_DAYS    = 95,
  ONE_ECG_FILE               = 96,
  STUDY_HOURS                = 97,
  SAMPLE_RATE                = 101,
  REPORT_PRE_TIME            = 105,
  REPORT_POST_TIME           = 106,
  DIGITAL_HP_FILTER          = 111,
  DIGITAL_LP_FILTER          = 113,
  DIGITAL_NOTCH_FILTER       = 114,
  NAG_ON_REPLACE_ELECTRODE   = 144,
  NAG_ON_LOW_BATTERY         = 145,
  PATCH_MAX_WEAR_LENGTH      = 146,
  ALLOW_LOW_BATTERY_START    = 147,
  STUDY_COMPLETE_SLEEP       = 148,
  NAG_ON_LEAD_OFF            = 149,
  BULK_UPLOAD                = 155,
  TRANSMIT_INTERVAL_REPORTS  = 156,
  DEMO_MODE                  = 160,
  MODEM_TEMPERATURE          = 171,
  PATIENT_ID                 = 201,
  SERVER_ADDRESS             = 213,
  TLS_IDENTITY               = 214,
  TLS_PSK                    = 215,
  APN_ADDRESS                = 216,
  APN_USERNAME               = 217,
  APN_PASSWORD               = 218,
  CONNECTION_TIMEOUT         = 225,
  ERROR_RETRIES              = 251,
  ERROR_PERIOD               = 252,
  ZYMED_COMPAT               = 253,
  BAD_TAG                    = 254,
  TERM_TAG                   = 255
};



///////////////////////////////////////////////////////////////////////////////
//       Class Definition
///////////////////////////////////////////////////////////////////////////////

static uint32_t line_number;


class _setting
{
  private:
    uint64_t min, max;
    uint64_t dataVal;
    settingTags tag;
    uint8_t maxLength;
    uint8_t dataLength;
    uint8_t binaryFlag;
    uint8_t setFlag;
    uint8_t minFW;
    uint8_t maxFW;
    string dataStr;

  public:
     _setting(const char *pS, settingTags tg, uint64_t mn, uint64_t mx, uint8_t len,
        uint8_t min_fw, uint8_t max_fw)
    {
      parseStr.assign(pS);
      tag = tg;
      min = mn;
      max = mx;
      dataVal = 0;
      maxLength = len;
      dataLength = len;
      dataStr.assign("");
      binaryFlag = 1;
      setFlag = 0;
      minFW = min_fw;
      maxFW = max_fw;
    }

    _setting(const char *pS, settingTags tg, uint8_t len,
        uint8_t min_fw, uint8_t max_fw)
    {
      parseStr.assign(pS);
      tag = tg;
      min = 0;
      max = 0;
      dataVal = 0;
      maxLength = len;
      dataLength = 0;
      dataStr.assign("");
      binaryFlag = 0;
      setFlag = 0;
      minFW = min_fw;
      maxFW = max_fw;
    }

    string parseStr;

    int set(string dS, bool check_range){
      if(binaryFlag){
        stringstream argStream;
        argStream << dS;
        argStream >> dataVal;
        dataStr.assign("");
        if( check_range && ((dataVal < min) || (dataVal > max)) ){
          cerr << line_number << ": " << "ERROR! Invalid binary data: setting" << parseStr << dataVal << endl;
          return -1;
        }

      }
      else{
        dataStr.assign(dS);
        dataVal = 0;
        dataLength = 0;
        if( check_range && (dataStr.length() > maxLength) ){
          cerr << line_number << ": " << "ERROR! Invalid string data: setting." << parseStr << dataStr << endl;
          cerr << line_number << ": " << "---Max Length: " << (int) maxLength << endl;
          cerr << line_number << ": " << "---Input Length: " << (int) dataStr.length() << endl;
          return -1;
        }
      }

      setFlag = 1;
      return 0;
    }

    int write(uint8_t *pBuffer, bool badAlignment, uint8_t firmwareVersion){
      int i = 0;
      uint32_t j;

      if(setFlag && (firmwareVersion >= minFW) && (firmwareVersion <= maxFW) ){
        pBuffer[i++] = tag;

        if(!binaryFlag){
          pBuffer[i++] = dataStr.length();

          for(j = 0; j < dataStr.length(); j++){
            pBuffer[i++] = dataStr[j];
          }
        }
        else{
          pBuffer[i++] = dataLength;

          uint64_t temp = dataVal;
          for(j = 0; j < dataLength; j++){
            pBuffer[i++] = temp & 0xff;
            temp = temp >> 8;
          }
        }

        if(badAlignment) pBuffer[i++] = 0xc4;
        else pBuffer[i++] = '\0';
      }

      return i;
    }

    int get_tag(){
      return (int) tag;
    }
};

_setting settingList[] = {
  _setting(".detect_pacemaker=", DETECT_PACEMAKER, 0, 1, 1, 10, 255),
  _setting(".pedometer_period=", PEDOMETER_PERIOD, 0, 900, 2, 10, 255),
  _setting(".store_raw_accel=", STORE_RAW_ACCEL, 0, 1, 1, 10, 255),
  _setting(".length_is_calendar_days=", LENGTH_IS_CALENDAR_DAYS, 0, 1, 1, 13, 255),
  _setting(".one_ecg_file=", ONE_ECG_FILE, 0, 1, 1, 10, 255),
  _setting(".study_hours=", STUDY_HOURS, 1, 24*31, 2, 10, 255),
  _setting(".sample_rate=", SAMPLE_RATE, 200, 1600, 2, 10, 255),
  _setting(".digital_hp_filter=", DIGITAL_HP_FILTER, 0, 100, 1, 10, 255),
  _setting(".digital_lp_filter=", DIGITAL_LP_FILTER, 0, 100, 1, 10, 255),
  _setting(".digital_notch_filter=", DIGITAL_NOTCH_FILTER, 0, 100, 1, 10, 255),
  _setting(".nag_on_replace_electrode=", NAG_ON_REPLACE_ELECTRODE, 0, 900,  2, 13, 255),
  _setting(".nag_on_low_battery=", NAG_ON_LOW_BATTERY, 0, 900,  2, 13, 255),
  _setting(".electrode_max_wear_hours=", PATCH_MAX_WEAR_LENGTH, 0, 168, 1, 13, 255),
  _setting(".allow_low_battery_start=", ALLOW_LOW_BATTERY_START, 0, 1, 1, 10, 255),
  _setting(".study_complete_sleep=", STUDY_COMPLETE_SLEEP, 0, 900, 2, 13, 255),
  _setting(".nag_on_lead_off=", NAG_ON_LEAD_OFF, 0, 900,  2, 13, 255),
  _setting(".demo_mode=", DEMO_MODE, 0, 2, 1, 10, 255),
  _setting(".patient_id=", PATIENT_ID, 40, 10, 255),
  _setting(".error_retries=", ERROR_RETRIES, 0, 250, 1, 10, 255),
  _setting(".error_period=", ERROR_PERIOD, 1, 1440, 2, 10, 255),
  _setting(".zymed_compat=", ZYMED_COMPAT, 0, 1, 1, 13, 255),
  _setting(".bad_tag=", BAD_TAG, 0, 1, 1, 10, 255)
};



///////////////////////////////////////////////////////////////////////////////
//       Gloabal Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
//    crcBlock()
//
//    This function calculates the CRC-CCITT value for a block of data, given
//    a seed value and the number of bytes to process.
//-----------------------------------------------------------------------------
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;
  for(j = 0; j < length; j++){
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }
  return 0;
}



///////////////////////////////////////////////////////////////////////////////
//       Public Function
///////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------
//    main()
//
//    This function reads in a file from cin and parses out the events.
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef WIN32
  _setmode (_fileno (stdout), O_BINARY);
#endif

  fstream argumentFile;
  istream *input;

  if(argc >= 2){
    argumentFile.open(argv[1], ios::in);
    input = &argumentFile;
  }
  else{
    input = &cin;
  }

  uint16_t firmwareVersion = 0;
  string serialNumber("");
  string deviceID("TZMR\0\0");
  uint16_t fileID = 0;
  bool checkInputData = 0;
  bool badAlignment = 0;
  bool badCRC = 0;
  bool badLength = 0;

  line_number = 0;

  // Loop through the entire input
  while(!input->eof()){
    string argument;
    char cstr[1024];

    // Get a single line from the input (max length: 1024 characters)
    input->getline(cstr, 1024);
    argument.assign(cstr);
    line_number++;

    // Trim any leading whitespace
    const size_t begStr = argument.find_first_not_of(" \t");
    if(begStr != string::npos) argument.erase(0, begStr);

    // Trim any trailing whitespace
    const size_t endStr = argument.find_last_not_of(" \n\r\t");
    if(endStr != string::npos) argument.resize(endStr+1);

    // Skip the line if it is a comment
    if(argument.find("#") == 0){
      continue;
    }

    // Only process lines that have more than 8 characters in them
    if(argument.length() > 8){
      // Trim whitespace before or after the "="
      bool done = 0;
      while(!done){
        size_t found;
        if((found = argument.find(" =")) != string::npos){
          argument.erase(found, 1);
        }
        else if((found = argument.find("\t=")) != string::npos){
          argument.erase(found, 1);
        }
        else if((found = argument.find("= ")) != string::npos){
          argument.erase(found+1, 1);
        }
        else if((found = argument.find("=\t")) != string::npos){
          argument.erase(found+1, 1);
        }
        else{
          done = 1;
        }
      }

      // Locate and parse any file-related parameters
      if(argument.find("file.") != string::npos){
        // Sets the firmware version embedded in the header of the file
        if(argument.find("firmware_version=") != string::npos){
          stringstream argStream;
          string tempStr = argument.substr(argument.find("=")+1);
          if(tempStr.length()){
            argStream << tempStr;
            argStream >> firmwareVersion;
          }
        }
        // Sets the serial number embedded in the header of the file
        else if(argument.find("serial_number=") != string::npos){
          serialNumber.assign(argument.substr(argument.find("=")+1));
#if 0
          if(serialNumber.length() && (serialNumber.length() != 7)){
            cerr << line_number << ": " << "ERROR! Invalid Serial Number: file.serial_number=" << serialNumber << endl;
            return -1;
          }
#endif
        }
        // Sets the file ID embedded in the header of the file
        else if(argument.find("file_id=") != string::npos){
          stringstream argStream;
          string tempStr = argument.substr(argument.find("=")+1);
          if(tempStr.length()){
            argStream << tempStr;
            argStream >> fileID;
          }
        }
        // Enables verification that settings are within valid ranges
        else if(argument.find("check_setting_ranges=") != string::npos){
          string tempStr(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            if(tempStr.find("true") != string::npos){
              checkInputData = 1;
            }
            else if(tempStr.find("false") != string::npos){
              checkInputData = 0;
            }
            else{
              cerr << line_number << ": " << "ERROR! Invalid string: file.check_setting_ranges=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if(argument.find("insert_bad_alignment=") != string::npos){
          string tempStr(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            if(tempStr.find("true") != string::npos){
              badAlignment = 1;
            }
            else if(tempStr.find("false") != string::npos){
              badAlignment = 0;
            }
            else{
              cerr << line_number << ": " << "ERROR! Invalid string: file.insert_bad_alignment=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if(argument.find("insert_bad_crc=") != string::npos){
          string tempStr(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            if(tempStr.find("true") != string::npos){
              badCRC = 1;
            }
            else if(tempStr.find("false") != string::npos){
              badCRC = 0;
            }
            else{
              cerr << line_number << ": " << "ERROR! Invalid string: file.insert_bad_crc=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Enables verification that settings are within valid ranges
        else if(argument.find("insert_bad_length=") != string::npos){
          string tempStr(argument.substr(argument.find("=")+1));
          if(tempStr.length()){
            if(tempStr.find("true") != string::npos){
              badLength = 1;
            }
            else if(tempStr.find("false") != string::npos){
              badLength = 0;
            }
            else{
              cerr << line_number << ": " << "ERROR! Invalid string: file.insert_bad_length=" << tempStr << endl;
              return -1;
            }
          }
        }
        // Abort if an invalid file setting is detected
        else{
          cerr << line_number << ": " << "ERROR! Unrecognized input: " << argument << endl;
          return -1;
        }
      }
      // Locate and process any settings from the file
      else if(argument.find("setting.") != string::npos){
        uint32_t i;
        // Find any matches with the setting array defined at the beginning of
        // this source file.
        bool matchFound = 0;
        for(i = 0; i < (sizeof(settingList)/sizeof(_setting)); i++){
          if(argument.find(settingList[i].parseStr) != string::npos){
            if(settingList[i].set(argument.substr(argument.find("=")+1),checkInputData)){
              return -1;
            }
            matchFound = 1;
            break;
          }
        }
        // We have to match a handful of settings manually.
        if(!matchFound){
          cerr << line_number << ": " << "ERROR! Unrecognized input: " << argument << endl;
          return -1;
        }
      }
      // Error on unrecognized settings - don't just skip them!
      else{
        cerr << line_number << ": " << "ERROR! Unrecognized input: " << argument << endl;
        return -1;
      }
    }
  }

  unsigned int length;                         // The length read from the file
  unsigned char buffer[MAX_FILE_LENGTH];       // Use a big ole buffer for the file
  uint32_t i;

  memset(buffer, 0, MAX_FILE_LENGTH);

  i = 6;                                       // The first 6 bytes are length and CRC

  strncpy((char *) &buffer[i], SETTINGS_ID_STRING, 6);  // The format identifier string
  i += 6;

  strncpy((char *) &buffer[i], MODEL_DESC_STRING, 6);   // The device ID string
  i += 6;

  buffer[i++] = firmwareVersion;                // This byte is Firmware Version (*10)

  strncpy((char *) &buffer[i], serialNumber.c_str(), 11);      // The next 11 bytes are the serial number
  i += 11;

  unsigned char *caster = (unsigned char *) &fileID;

  buffer[i++] = caster[0];
  buffer[i++] = caster[1];

  uint32_t j;

  for(j = 0; j < (sizeof(settingList)/sizeof(_setting)); j++){
    i += settingList[j].write(&buffer[i], badAlignment, firmwareVersion);
  }


  buffer[i++] = TERM_TAG;
  buffer[i++] = 1;
  buffer[i++] = 255;
  buffer[i++] = '\0';

  length = i;

  i = 2;
  uint32_t temp = length;
  if(badLength) temp += 255;
  for(j = 0; j < 4; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  uint16_t crcValue = 0xffff;

  crcBlock(&buffer[2], length - 2, &crcValue);

  i = 0;
  temp = crcValue;
  if(badCRC) temp++;
  for(j = 0; j < 2; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  cout.write((const char *) buffer, length);

  return 0;
}




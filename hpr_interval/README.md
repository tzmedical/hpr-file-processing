# HPR INTERVAL FILE SPECIFICATION #

## Overview ##

In addition to the transfer of raw ECG data, the HPR will provide hourly interval repoort files containing device status and atient-activated events. These files will each contain data pertaining to one hour of ECG recording.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZINT" to identify the file as an actions file |
| Device String | 6 | The ASCII string "HPR" to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| Patient ID | 40 | ASCII String identifier unique to patient |
| Entry 1 | *Variable* | First event entry. Format described in the events README |
| ... | ... | ... |
| Entry N | *Variable* | Last event entry. Format described in the events README |
| Terminator | 20 | Terminator entry. Format described below |

## Event Entry Format ##

Each entry in the actions file will contain an instruction for one action to be executed by the HPR. These entries will consist of an identifier byte, a data length byte, a variable number of data bytes, and a reserved byte for internal use by the HPR. The values of the identifier tags may be found below. Each entry will adhere to the following format:

| Item | Bytes | Description |
| --- | --- | --- |
| Event Tag | 1 | This value identifies the event associated with the data to follow. Possible values are described in the events README. |
| Sequence Number | 4 | This is the ECG sequence number of the file associated with this event, corresponding to tag 31 on page 34 of the SCP-ECG Standard version 2.2 |
| Sample Count | 2 | Number of samples since beginning of SCP file |
| Date | 4 | This is the date of the entry following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Time | 3 | This is the time of the entry (using UTC), following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Milliseconds | 1 | This is the number of 4 millisecond time units to pass since the last second change, to provide better distinction between events |
| Time Zone | 2 | This is the number of minutes that local time is offset from UTC. Valid range is -780 to 780 |
| Data Length | 1 | This is the number of bytes of data associated with this event |
| Data | <_length_> | The value of this section is dependent on the value of the tag. Refer to the events README |
| Reserved | 1 | Null byte. This is a '\0' used to identify the end of each entry |

## Event Terminator Entry ##

The final entry in each interval report file will be a terminator entry. This entry follows the same format as other entries, with a tag of **255** and data of **255**. The format is shown below:

| Item | Bytes | Description |
| --- | --- | --- |
| Tag | 255 | This value will be 255 |
| Sequence Number | 4 | This will be the ECG sequence number at the time of closing |
| Sample Count | 2 | This will be the sample number at time of closing |
| Date | 4 | This is the date that the file was written |
| Time | 3 | This is the time (using UTC) that the file was written |
| Milliseconds | 1 | Milliseconds/4 (0 - 250) |
| Time Zone | 2 | Time zone when file was written |
| Data Length | 1 | The value of this section will be 1 |
| Data | 1 | The value of this section will be 255 (0xFF) |
| Reserved | 1 | Null byte. This is a '\0' used to identify the end of each entry |

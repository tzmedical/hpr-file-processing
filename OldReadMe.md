# HPR Device File Processing #

This repository contains a collection of programs written in C++ to help speed development of programs for interacting with the binary files generated and consumed by TZ Medical Clarus devices. These example programs were developed and tested in Cygwin, but should be easily adaptable to other Windows or Linux development environments.

To expediate development of software this repository also includes specifications for the File Formats used by the HPR device, including the Actions, Events, and Settings generated and consumed by TZ Medical Clarus devices.

## Communication Methods ##

Data may be transfered between the PC and the HPR device via direct access through provided USB docks. The file system layout is outline below.

### File System for SD Card ###

The HPR stores all the data it records on an SDHC card using a FAT file system. The files are organized into directories and sub-directories to optimize device access to files. The naming conventions are as follows:

| File Type | Naming Convention | Description |
| --- | --- | --- |
| SCP-ECG Data | ecgs/SS/SS/SS/SS.scp | Where SSSSSSSS is the 8-digit sequence number of each SCP-ECG file |
| Event Reports |events/SS/SS/SS/SS_CCC.tze | Where SSSSSSSS is the 8-digit sequence number and CCC is the event number |
| Interval Reports | tzr/YYYMMDD/HH.tzr | Where YYYY is the year, MM is the month, DD is the day, and HH is the hour of the report, referenced to UTC |
| Settings File | config.tzs | This file will always have the same name. If this file does not exist, the device will search for other files with the \*.tzs suffix and will rename the first one it finds to config.tzs (filling in un-set settings with what is stored interally on the device). If no such file is found, the device will generate a new config.tzs using default values stored interally to the device |
| Actions File | \*.tza | On bootup, the device will search the root directory for a file with the \*.tza suffix, execute the contents, and then delete the file |

## Included Programs for Processing HPR Files ##

The following programs are included in the repository:

### hpr_actions_generator ###
Use this program to generate binary actions files to send to the device.

### hpr_actions_parser ###
If the device is complaining about your actions files, you can you this program to parse binary actions files and output a text description for debugging purposes.

### h3r_event_finder ###
This program recursively searches a directory for any and all event files and outputs a 1-line summary of each event it finds.

### h3r_event_parser ###
Use this program to parse a single binary event file and output a text description of all the fields.

### h3r_interval_finder ###
This program recursively searches a directory for any and all interval files and outputs a 1-line summary for each event entry it finds within these files. Note: it does not check the header information (e.g. serial number, patient ID, etc.).

### h3r_interval_parser ###
Use this program to parse a single binary interval file and output a text description of all its contents.

### hpr_settings_generator ###
Use this program to generate a binary settings file for a Clarus device. We recommend using **h3r_settings_parser** to generate the input file for **h3r_settings_generator** to avoid confusion.

### hpr_settings_parser ###
Use this program to parse a binary settings file and output a text description of alll its contents in a format that can be used as an input to the **h3r_settings_generator** program.

### rtc_generator ###
Use this program to create a \*.rtc file to be placed on the HPR device to update the device's system time.

### one_ecg_splitter ###
Use this program to split an SCP-ECG file created by a device with the one_ecg_file set. The result will be a file tree matching the normal scp file tree on a device without one_ecg_file set.

### scp_parser ###
Use this program to parse a binary SCP-ECG file and output ONLY the header information as a text file.

### scp_stripper ###
Use this program to parse a binary SCP-ECG file and output ONLY the ECG waveform data as a text *.csv file.

### hpr_accel_reader ###
Use this program to parse TZG files and input the header and/or the accelerometer data as a text file.

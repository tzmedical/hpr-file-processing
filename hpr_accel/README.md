# HPR ACCELEROMETER FILE SPECIFICATION #

## Overview ##

The purpose of this document is to list each section of the tzg file format used for storing HPR acceleration data.

## Overall File Layout ##

| Item | Bytes | Description |
| --- | --- | --- |
| CRC | 2 | CRC-CCITT of the entire file, excluding these two bytes |
| Length | 4 | Length of the file in bytes, including the CRC |
| Format String | 6 | The ASCII string "TZACC" to identify the file as an accelerometer file |
| Device String | 6 | The ASCII string "HPR" to identify the target device |
| Firmware Version | 1 | Version of the firmware, with 10 = 1.0 and 22 = 2.2, etc |
| Serial Number | 11 | ASCII String of the serial number of the device, NULL Terminated |
| Patient ID | 40 | ASCII String identifier unique to patient |
| Sequence Number | 4 | This is the ECG sequence number of the file associated with this acceleration data, corresponding to tag 31 on page 34 of the SCP-ECG Standard version 2.2 |
| Number of Axes | 1 | The number of axes for which data is stored in this file |
| Date | 4 | This is the date of the entry following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Time | 3 | This is the time of the entry (using UTC), following the format outlined on page 33 of the SCP-ECG Standard version 2.2 |
| Milliseconds | 1 | This is the number of 4 millisecond time units to pass since the last second change |
| Time Zone | 2 | This is the number of minutes that local time is offset from UTC. Valid range is -780 to 780 |
| Number of Samples | 4 | The number of samples of accelerometer data for each stored axes |
| Data | <_Num\_Samples \* Num\_Axes_> | This is where all the accelerometer data is stored. See below for more information |

### Internal Data Format ###

All the data for one axis is formed in an array together before the next axis' data.

| Bytes        | First Sample | Second Sample | Third Sample | **...**  | Second to Last Sample | Last Sample |
| :---:        | :---:        | :---:         | :---:        | :---:    | :---:                 | :---:       |
| Num. Samples | x0           | x1            | x2           | **...**  | xn-1                  | xn          |
| Num. Samples | y0           | y1            | y2           | **...**  | yn-1                  | yn          |
| Num. Samples | z0           | z1            | z2           | **...**  | zn-1                  | zn          |

All axes have the same number of samples, and each sample is a single byte of data.
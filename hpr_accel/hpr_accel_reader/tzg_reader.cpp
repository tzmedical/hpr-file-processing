/******************************************************************************
 *       Copyright (c) 2017, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       tzg_reader.cpp
 *          - This program parses an *.tzg accelerometer file from -i <input> and outputs a
 *            text interpretation of the information on <cout>.
 *          - Required software:
 *                make
 *                g++
 *          - Build this program using make, default settings:
 *                make
 *          - call syntax is:
 *                ./tzg_parser -i input.scp [options] > output.txt
 *          -options include:
 *               -h : Include header info in output.
 *               -d : Include csv date in output.
 *
 *
 *
 *****************************************************************************/


///////////////////////////////////////////////////////////////////////////////
//       Include Files
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

///////////////////////////////////////////////////////////////////////////////
//       Private Definitions
///////////////////////////////////////////////////////////////////////////////


/***   Feature Defines   ***/
#define CHECK_CRC                   // Comment this line to disable CRC Checking

#define MIN_FILE_SIZE      (256)
#define MAX_FILE_SIZE      (256*1024)

#define HEADER_SIZE        (6)

#define EXPECTED_IDENT     "TZACC"

#define PATIENT_ID_LEN     (40)


///////////////////////////////////////////////////////////////////////////////
//       Private Variables
///////////////////////////////////////////////////////////////////////////////

unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


///////////////////////////////////////////////////////////////////////////////
//       Private Functions
///////////////////////////////////////////////////////////////////////////////

int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
  unsigned int j;

  if(!length)
  {
    cout << "Attempting to CRC 0 bytes! Aborting." << endl;
    return -1;
  }

  for(j = 0; j < length; j++)
  {
    *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
  }

  return 0;
}

unsigned char read8(unsigned char *pData, unsigned int * offset)
{
  unsigned char *pC = &pData[*offset];
  *offset += 1;
  return *pC;
}

unsigned short read16(unsigned char *pData, unsigned int * offset)
{
  unsigned char *pC = &pData[*offset];
  *offset += 2;
  return (unsigned short) pC[0] + (unsigned short) pC[1]*256;
}

unsigned int read32(unsigned char *pData, unsigned int * offset)
{
  unsigned char *pC = &pData[*offset];
  *offset += 4;
  return (((unsigned int)pC[3]*256 + (unsigned int)pC[2])*256 
      + (unsigned int)pC[1])*256 + (unsigned int) pC[0];

  
}

short readAccel(unsigned char *pData, unsigned int * offset)
{
    unsigned char firstByte = pData[*offset];
    unsigned char nextByte = pData[*offset + 1];
    *offset = *offset + 1;

    short result;

    if (firstByte & 0x80)
    {
        //1-Byte Value
        result = 0x003f & firstByte;

        if (firstByte & 0x40)
        {
            result = -result;
        }
    }
    else
    {
        //2-Byte Value
        result = ((0x003f & firstByte) << 6) + (0x003f & nextByte);
        *offset = *offset + 1;

        if (nextByte & 0x40)
        {
            result = -result;
        }
    }

    return result;
}

  ///////////////////////////////////////////////////////////////////////////////
  //       Public Functions
  ///////////////////////////////////////////////////////////////////////////////

  //-----------------------------------------------------------------------------
  //    main()
  //
  //    This function reads in a tzg file from inFile and parses out all of
  //    the header information, ignoring the actual ECG data.
  //-----------------------------------------------------------------------------
  int main(int argc, char *argv[])
  {
    string fileName;

    unsigned short calculatedCrcValue;           // The CRC we calculate
    unsigned short theircrcValue;                // The CRC from the file
    unsigned int fileLength;                     // The length read from the file
    unsigned char * fBuff = NULL;                // Pointer used for the read command
    unsigned int bufferAddress = 0;

    //File Structure Elements
    string fileID;
    string deviceID;
    unsigned short firmwareVersion;
    string serialNumber;
    string patientID;

    unsigned int sequenceNumber;
    unsigned char numAxes;

    unsigned short dateYear;
    unsigned char dateMonth;
    unsigned char dateDay;
    unsigned char timeHour;
    unsigned char timeMinute;
    unsigned char timeSecond;
    short timeZone;

    unsigned int numSamples;

    unsigned char header[HEADER_SIZE];
    int i;
    int j;

    bool includeHeader = false;
    bool includeData = false;

    // Process the command line arguments
    for (i = 0; i < argc; i++) 
    {
        if (*argv[i] == '-') switch (*(argv[i] + 1)) 
        {
        case 'i':
            if (++i >= argc) 
            {
                cerr << ": filename must follow -i." << endl;
                return -1;
            }
            fileName.assign(argv[i]);
        break;

        case 'h':
            includeHeader = true;
        break;

        case 'd':
            includeData = true;
        break;

        default:
            cerr << ": Unexpected option: " << *(argv[i] + 1) << endl;
            cerr << "Aborting (3)." << endl;
            return -1;
        }
    }

    if(argc < 2)
    {
      cerr << "No input file specified! Aborting." << endl;
      return -1;
    }

    // Open the file and check the file length
    fstream inFile (fileName, ios::in | ios::binary);
    inFile.seekg(0, ios::end);

    fileLength = inFile.tellg();

    inFile.seekg(0, ios::beg);

    if ((int) fileLength == -1)
    {
        cerr << "File doesn't exist. Aborting." << endl;
        return -1;
    }

    // Make sure the file is not too big for the format
    if(fileLength > MAX_FILE_SIZE)
    {
      cerr << "File size is SIGNIFICANTLY larger than expected. Aborting." << endl;
      return -1;
    }

    // Make sure the file is not too small for the format
    if(fileLength < MIN_FILE_SIZE)
    {
      cerr << "File size is too small for an TZG file. Aborting." << endl;
      return -1;
    }

    //Read the header information--------------------------------------------------------------------------------

    // Read in the first TWO 16 byte blocks to see if the file has been encrypted
    for(i = 0; i < HEADER_SIZE; i++)
    {
      header[i] = 0;
    }

    inFile.read((char *) header, HEADER_SIZE);

    //TODO: Add check to verify we're reading a TZG file

    //Check the CRC----------------------------------------------------------------------------------------------
    theircrcValue = read16(header, &bufferAddress);
    fileLength = read32(header, &bufferAddress);

    fBuff = new unsigned char [fileLength];          // Allocate enough space to read in the whole file  

    for(i = 0; i < HEADER_SIZE; i++)
    {
      fBuff[i] = header[i];                 // Copy the first block into the file buffer
    }

    inFile.read((char *) &fBuff[HEADER_SIZE], fileLength-HEADER_SIZE);    // Store the remainder of the file in memory

    calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
    crcBlock(&fBuff[2], fileLength-2, &calculatedCrcValue); // Calculate the CRC for the remainder of the file

#ifdef CHECK_CRC
    if(calculatedCrcValue == theircrcValue)
    {           
      // If the CRC doesn't match, something has gone wrong
      //cout << "File CRC Valid: 0x" << hex << theircrcValue << dec << endl;  // Notify user that we are parsing
#else
      cout << "Ignoring CRC (0x" << hex << theircrcValue 
        << " : 0x" << calculatedCrcValue << ")" << dec << endl;  // Ignore the CRC values (debugging ONLY)
#endif

      //Check file info
      fileID = string((const char*) &fBuff[bufferAddress]);
      bufferAddress += fileID.length() + 1;

      if (fileID.compare(EXPECTED_IDENT))
      {
          cerr << "Unexpected File Identifier: \"" << fileID << "\". Aborting." << endl;
          return -1;
      }

      deviceID = string((const char*) &fBuff[bufferAddress]);
      bufferAddress += deviceID.length() + 1;

      firmwareVersion = read8(fBuff, &bufferAddress);

      serialNumber = string((const char*)&fBuff[bufferAddress]);
      bufferAddress += serialNumber.length() + 1;

      patientID = string((const char*) &fBuff[bufferAddress]);
      bufferAddress += PATIENT_ID_LEN;

      sequenceNumber = read32(fBuff, &bufferAddress);

      numAxes = read8(fBuff, &bufferAddress);
      dateYear = read16(fBuff, &bufferAddress);
      dateMonth = read8(fBuff, &bufferAddress);
      dateDay = read8(fBuff, &bufferAddress);
      timeHour = read8(fBuff, &bufferAddress);
      timeMinute = read8(fBuff, &bufferAddress);
      timeSecond = read8(fBuff, &bufferAddress);

      //There's a junk byte in the structure. 
      //Probably an attempt at byte alignment.
      bufferAddress++;

      timeZone = read16(fBuff, &bufferAddress);
      numSamples = read32(fBuff, &bufferAddress);

      cout << "Num Samples: " << numSamples << endl;

      char accelData[numAxes][numSamples];

      for (i = 0; i < numAxes; i++)
      {
          for (j = 0; (unsigned int) j < numSamples; j++)
          {
              accelData[i][j] = read8(fBuff, &bufferAddress);
          }
      }      

      //Print Header Information
      if (includeHeader)
      {
          cout << "File Type: " << fileID << endl;
          cout << "Device ID: " << deviceID << endl;
          cout << "Device SN: " << serialNumber << endl;
          cout << "Device FW: " << firmwareVersion << endl;
          cout << "\n" << endl;
          cout << "Patient ID: " << patientID << endl;
          cout << "Sequence Number: " << setfill('0') << setw(8) << sequenceNumber << endl;
          cout << "File Start Time: " << setw(2) << (unsigned int)dateMonth;
          cout << "/" << setw(2) << (unsigned int)dateDay << "/";
          cout << setw(4) << (unsigned int)dateYear << " " << setw(2) << (unsigned int)timeHour;
          cout << ":" << setw(2) << (unsigned int)timeMinute << ":";
          cout << setw(2) << (unsigned int)timeSecond << " UTC";
          cout << setw(2) << showpos << (timeZone / 60) << noshowpos << endl;
      }

      if (includeData)
      {
          cout << "Sample,Axis 1,Axis 2,Axis 3" << endl;

          //Print the rest of the data as CSV.
          for (i = 0; (unsigned int)i < numSamples; i++)
          {
              cout << i << "," << +accelData[0][i] << "," << +accelData[1][i] << "," << +accelData[2][i] << endl;
          }
      }
      
#ifdef CHECK_CRC
    }
    else
    {
      cerr << "File CRC Invalid. Halting\n";
      cerr << "their CRC: " << theircrcValue << " - our CRC: " << calculatedCrcValue << endl;
      cerr << "File Length: " << fileLength << endl;            // Print out the file length      
    }
#endif

    return 0;
  }


